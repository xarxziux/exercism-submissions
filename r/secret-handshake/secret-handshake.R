reverse.codes <- function (num)
  (num %% 32) >= 16

num.to.bool <- function (num)
  c(
    (num %% 2) > 0,
    (num %% 4) >= 2,
    (num %% 8) >= 4,
    (num %% 16) >= 8
  )

codes <- c("wink", "double blink", "close your eyes", "jump")

handshake <- function(n) {
  answer <- codes[num.to.bool (n %% 32)]

  if (length (answer) == 0)
    NULL
  else if (reverse.codes (n))
    rev (answer)
  else
    answer
}
