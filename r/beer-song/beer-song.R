beer <- " of beer"
on_the_wall <- " on the wall"
take_one <- " down and pass it around, "
more_beer <- "Go to the store and buy some more, 99 bottles of beer on the wall.\n"

str.bottle <- function (num) {
  if (num == 1)
    "bottle"
  else
    "bottles"
}

str.count <- function (num) {
  if (num == 0)
    "no more "
  else
    paste (num, " ", sep = "")
}

str.pronoun <- function (num) {
  if (num == 1)
    "Take it"
  else
    "Take one"
}

capitalise <- function (x) {
  if (substr (x, 1, 7) == "no more")
    sub ("n", "N", x)
  else
    x
}

verse <- function(number) {

  b <- paste (
    str.count (number),
    str.bottle (number),
    beer,
    sep = ""
  )

  last_line <- if (number == 0)
      more_beer
    else
      paste (
        str.pronoun (number),
        take_one,
        str.count (number - 1),
        str.bottle (number - 1),
        beer,
        on_the_wall,
        ".\n",
        sep = ""
      )

  paste (
    capitalise (b),
    on_the_wall,
    ", ",
    b,
    ".\n",
    last_line,
    sep = ""
  )
}

lyrics <- function(first, last) {
  paste (sapply (c (first:last), verse), collapse = "\n")
}


