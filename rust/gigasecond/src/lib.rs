/*
extern crate chrono;
use chrono::{DateTime, Utc};
use time::Duration;

// Returns a Utc DateTime one billion seconds after start.
pub fn after(start: DateTime<Utc>) -> DateTime<Utc> {
    start + Duration::from_secs (1000000000)
}
*/

extern crate chrono;
extern crate time;

fn main() {
    use chrono::prelude::*;
    use time::Duration;

    let dt = Utc::now() + Duration::days(137);

    println!("{}", dt);
}
