fn get_row_max (m: &[Vec<u64>], c: usize) -> u64 {
    *m[c].iter().max().unwrap()
}

fn get_col_min (m: &[Vec<u64>], c: usize) -> u64 {
    m.iter().map(|x| x[c]).min().unwrap()
}

pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let mut answer: Vec<(usize, usize)> = Vec::new();

    if input.len() > 0 && input[0].len() > 0 {
        for row in 0 .. input.len() {
            let row_max: u64 = get_row_max(input, row);

            for col in 0 .. input[0].len() {
                let elem: u64 = input[row][col];
                let col_min: u64 = get_col_min(&input, col);

                if elem == row_max && elem == col_min {
                    answer.push((row, col));
                }
            }
        }
    }

    answer
}
