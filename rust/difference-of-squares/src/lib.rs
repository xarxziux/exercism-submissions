pub fn square_of_sum(n: usize) -> usize {
    usize::pow((1..=n).sum(), 2)
}

pub fn sum_of_squares(n: usize) -> usize {
    (1..=n).map(|x| x * x).sum()
}

pub fn difference(n: usize) -> usize {
    square_of_sum(n) - sum_of_squares(n)
}
