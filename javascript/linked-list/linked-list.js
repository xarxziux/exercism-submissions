const createNode = (value, prev, next) => ({value, prev, next});

export default class LinkedList {
    constructor() {
        this.clearList();
    }

    clearList() {
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    unshift(value) {
        const newNode = createNode(value, null, this.first);

        if (this.size === 0) {
            this.last = newNode;
        } else {
            this.first.prev = newNode;
        }

        this.first = newNode;
        this.size += 1;
    }

    push(value) {
        const newNode = createNode(value, this.last, null);

        if (this.size === 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }

        this.last = newNode;
        this.size += 1;
    }

    shift() {
        const first = this.first;

        if (this.size === 1)
            this.clearList();
        else {
            this.first = this.first.next;
            this.first.prev = null;
            this.size -= 1;
        }

        return first.value;
    }

    pop() {
        const last = this.last;

        if (this.size === 1)
            this.clearList();
        else {
            this.last = this.last.prev;
            this.last.next = null;
            this.size -= 1;
        }

        return last.value;
    }

    delete(value) {
        let elem = this.first;

        while (elem !== null) {
            if (elem.value === value) {
                if (this.size === 1)
                    this.clearList();
                else if (elem === this.first) {
                    this.first = this.first.next;
                    this.first.prev = null;
                    this.size -= 1;
                } else if (elem === this.last) {
                    this.last = this.last.prev
                    this.last.next = null;
                    this.size -= 1;
                } else {
                    elem.prev.next = elem.next;
                    elem.next.prev = elem.prev;
                    this.size -= 1;
                }
            }

            elem = elem.next;
         }
    }

    flatten() {
        let elem = this.first;
        let answer = [];

        while (elem !== null) {
            answer.push(elem.value);
            elem = elem.next;
        }

        return answer;
    }

    count() {
        return this.size;
    }
};
