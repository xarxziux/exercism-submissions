/** @format */

/*
 * Given that the module only exports a single function, the original
 * design of this test - expecting a default export of a class - makes no
 * sense.  Therefore I've fixed it to accept a single, named function.
 * The result reduces both complexity and code size.
 */

import {annotate} from './minesweeper';

describe('annotate()', () => {

    test('handles no rows', () => {

        expect(annotate([])).toEqual([]);

    });

    test('handles no columns', () => {

        expect(annotate([''])).toEqual(['']);

    });

    test('handles no mines', () => {

        const input = ['   ', '   ', '   '];
        const expected = ['   ', '   ', '   '];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles board with only mines', () => {

        const input = ['***', '***', '***'];
        const expected = ['***', '***', '***'];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles mine surrounded by spaces', () => {

        const input = ['   ', ' * ', '   '];
        const expected = ['111', '1*1', '111'];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles space surrounded by mines', () => {

        const input = ['***', '* *', '***'];
        const expected = ['***', '*8*', '***'];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles space surrounded by mines', () => {

        const input = ['***', '* *', '***'];
        const expected = ['***', '*8*', '***'];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles horizontal line', () => {

        const input = [' * * '];
        const expected = ['1*2*1'];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles horizontal line, mines at edges', () => {

        const input = ['*   *'];
        const expected = ['*1 1*'];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles vertical line', () => {

        const input = [' ', '*', ' ', '*', ' '];
        const expected = ['1', '*', '2', '*', '1'];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles vertical line, mines at edges', () => {

        const input = ['*', ' ', ' ', ' ', '*'];
        const expected = ['*', '1', ' ', '1', '*'];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles cross', () => {

        const input = ['  *  ', '  *  ', '*****', '  *  ', '  *  '];
        const expected = [' 2*2 ', '25*52', '*****', '25*52', ' 2*2 '];
        expect(annotate(input)).toEqual(expected);

    });

    test('handles large board', () => {

        const input = [
            ' *  * ',
            '  *   ',
            '    * ',
            '   * *',
            ' *  * ',
            '      '
        ];
        const expected = [
            '1*22*1',
            '12*322',
            ' 123*2',
            '112*4*',
            '1*22*2',
            '111111'
        ];
        expect(annotate(input)).toEqual(expected);

    });

});
