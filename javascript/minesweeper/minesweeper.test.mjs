/** @format */

import {annotate, countNearbyMines, arrToGrid} from './minesweeper';

const input = [' *  * ', '  *   ', '    * ', '   * *', ' *  * ', '      '];
const expectedAns = [
  '1*22*1',
  '12*322',
  ' 123*2',
  '112*4*',
  '1*22*2',
  '111111'
];

//console.log(annotate(input));

//const g = arrToGrid(input);
//console.log(countNearbyMines(g)(0, 30));
console.log(input.map(str => str.split('')));
