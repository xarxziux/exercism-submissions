/** @format */

const getCoordsArr = width => [
  index => index - width,
  index => (index + 1) % width == 0 ? null : index + 1 - width,
  index => (index + 1) % width == 0 ? null : index + 1,
  index => (index + 1) % width == 0 ? null : index + 1 + width,
  index => index + width,
  index => index % width == 0 ? null : index - 1 + width,
  index => index % width == 0 ? null : index - 1,
  index => index % width == 0 ? null : index - 1 - width
];

const arrToGrid = arr => arr.map(str => str.split(''));

const gridToArr = ({grid, width}, accum = []) =>
  grid.length <= width
    ? accum.concat(grid.join(''))
    : gridToArr(
      {
        grid: grid.slice(width),
        width
      },
      accum.concat(grid.slice(0, width).join(''))
    );

const isMine = x => x === '*';

export const countNearbyMines = (grid, width) => (curr, index) => {

  const coordsArr = getCoords (width);

  return isMine(curr)
    ? '*'
    : 0 +
      isMine(grid[coordsArr[0](index)]) +
      isMine(grid[coordsArr[1](index)]) +
      isMine(grid[coordsArr[2](index)]) +
      isMine(grid[coordsArr[3](index)]) +
      isMine(grid[coordsArr[4](index)]) +
      isMine(grid[coordsArr[5](index)]) +
      isMine(grid[coordsArr[6](index)]) +
      isMine(grid[coordsArr[7](index)])
};
/*
const annotateEach = grid => (_, index) => {
  const countInternal = countNearbyMines(grid);

  if (isMine(grid.data[index])) return '*';

  const mineCount = countInternal(index);

  return mineCount === 0 ? ' ' : mineCount;
};
*/

const annotateAll = grid => ({
  data: grid.data.map(countNearbyMines(grid)),
  width: grid.width
});

export const annotate = arr =>
  // (arr.length === 0 ? [] : annotateAll(arrToGrid(arr)));
  // arr.length === 0 ? [] : arrToGrid(arr);
  arr.length === 0 ? [] : gridToArr(annotateAll(arrToGrid(arr)));
