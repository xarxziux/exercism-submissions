export default class Gigasecond {
  constructor (d) {
    this.d = new Date (d.getTime() + 10 ** 12);
  }
  date() {
    return this.d;
  }
}
