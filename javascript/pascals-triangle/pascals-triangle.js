// Memoized triangle array, so that we don't have to re-calculate it every time
const savedTriangle = [];

// Helper functions for build individual rows
const factorial = (n, a = 1) => (n === 0) ? a : factorial (n - 1, n * a);
const binomial = n => r => factorial(n)/(factorial(r) * factorial(n - r));
const allBinomials = n => Array(n).fill().map((_, i) => binomial(n - 1)(i));

// If we have already cached the triangle, return it
// Otherwise extend it
const buildTriangle = n => {
    let l = savedTriangle.length;

    if (n <= l)
        return savedTriangle.slice(0, n);

    for (; l < n; l += 1) {
        savedTriangle[l] = allBinomials(l + 1);
    }

    return savedTriangle;
};

// Now put it all together
export class Triangle {
    constructor(rows) {
        this.rows = buildTriangle(rows);
        this.lastRow = this.rows[this.rows.length - 1];
    }
}
