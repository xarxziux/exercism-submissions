/** @format */

const collatz = (current, count = 0) =>
    (current === 1
        ? count
        : current % 2 === 0
            ? collatz(current / 2, count + 1)
            : collatz(current * 3 + 1, count + 1));

export const steps = (x) => {

    const num = Math.round(x - 0);

    if (Number.isNaN(num)) throw new Error('Invalid input');

    if (x <= 0) throw new Error('Only positive numbers are allowed');

    return collatz(num);

};
