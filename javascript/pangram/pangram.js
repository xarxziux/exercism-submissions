/** @format */

// Here's the actual, one-line answer
const isPangramFn = str =>
    new Set(str.toLowerCase().replace(/[^a-z]/g, '')).size === 26;

// And here's the answer wrapped in a class in order to pass the unit tests
export default class Pangram {

    constructor (str) {

        this.isPangramProp = isPangramFn(str);

    }

    isPangram () {

        return this.isPangramProp;

    }

}

// Classes.  Why use 1 line when you can use 20?
