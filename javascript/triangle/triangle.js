/** @format */

const types = ['scalene', 'isosceles', '', 'equilateral'];

const isValidTriangle = (s1, s2, s3) =>

// Implicit coercion FTW!!!
const kindFn = (s1, s2, s3) =>
    types[0 + (s1 === s2) + (s1 === s3) + (s2 === s3)];

// Great. That's us all done.  Now we just need to export that...

// ... no, wait.  The unit tests insist on us wrapping a stateless
// function inside a class.  Sigh!

export default class Triangle {

    // There's no requirement to store the values of the sides.
    // So let's just work out the kind now and store that.
    constructor (...sides) {

        this.kindStr = kindFn(...sides);

    }

    kind () {

        return this.kindStr;

    }

}

// Classes.  3 lines of code squeezed into 11!
