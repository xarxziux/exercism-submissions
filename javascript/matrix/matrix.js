const strToRows = str =>
    str.split('\n').map(x => x.split(' ').map(x => x - 0));

const transpose = rows =>
    rows[0].map((_, i) => rows.map((_, j) => rows[j][i]));

export default class Matrix {
    constructor(str){
        this.rows = strToRows(str);
        this.columns = transpose(this.rows);
    }
}
