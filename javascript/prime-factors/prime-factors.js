const reduceBy = ({ base, factor, accum }) => (
  base % factor !== 0
    ? { base, accum }
    : reduceBy({
      base: base / factor,
      factor,
      accum: accum.concat(factor),
    })
);

const getFactors = (num) => {
  let { base, accum } = reduceBy({
    base: num,
    factor: 2,
    accum: [],
  });

  let factor = 3;

  while (base >= factor) {
    ({ base, accum } = reduceBy({
      base,
      factor,
      accum,
    }));

    factor += 2;
  }

  return accum;
};

export default class PrimeFactors {
  for(num) {
    return getFactors(num);
  }
}
