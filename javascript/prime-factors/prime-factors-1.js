/*
 * The trampolined version.
 * Stack safety is improved.
 * Readability... not so much.
 */
/* eslint-disable indent */
const findOddFactors = ({ current, accum, divisor }) => (
    current === 1
        ? { accum }
        : divisor * 2 > current
        ? { accum: accum.concat(current) }
        : current % divisor === 0
        ? {
            current: current / divisor,
            accum: accum.concat(divisor),
            divisor,
        }
        : {
            current,
            accum,
            divisor: divisor + 2,
        }
);

const trampoline = (initCurrent, initAccum) => {
    let current = initCurrent;
    let accum = [...initAccum];
    let divisor = 3;

    do {
        ({ current, accum, divisor } =
            findOddFactors({ current, accum, divisor }));
    } while (current != null);

    return accum;
};

const flattenEven = (current, accum = []) =>
    (current === 1
        ? accum
        : current % 2 === 0
        ? flattenEven(current / 2, accum.concat(2))
        : trampoline(current, accum));

export default class PrimeFactors {
    for(num) {
        return flattenEven(num);
    }
}
