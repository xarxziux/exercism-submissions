/** @format */

const throwError = (msg) => {
  throw new Error(msg);
};

const dnaMap = {
  G: 'C',
  C: 'G',
  T: 'A',
  A: 'U',
};

const translate = x => dnaMap[x];

export const toRna = dna =>
  (/^[ACGT]*$/.test(dna)
    ? dna.replace (/./g, translate)
    : throwError('Invalid input DNA.'));
