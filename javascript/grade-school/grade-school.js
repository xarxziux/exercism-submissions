export class GradeSchool {
    constructor() {
        this._roster = new Map();
    }

    add(name, grade) {
        if (this._roster.has(grade)) {
            const g = this._roster.get(grade);
            g.push(name);
            g.sort();
        } else {
            this._roster.set(grade, [name]);
        }
    }

    grade(grd) {
        return [...(this._roster.get(grd) || [])];
    }

    roster(){
        const answer = {};
        for(const [k, v] of this._roster)
            answer[k] = [...v];
        return answer;
    }
}
