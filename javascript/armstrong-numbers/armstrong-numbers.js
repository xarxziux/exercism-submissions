/** @format */

export const validate = x =>
    x ===
    `${x}`
        .split('')
        .map((e, _, {length}) => e ** length)
        .reduce((a, b) => a + b);
