export class List {
    constructor(input = []) {
        this.data = input;
    }

    get values() {
        return this.data;
    }

    append(l2) {
        return new List(this.data.concat(l2.values));
    }

    concat(l2) {
        return new List(flatten(this.data, l2.values));
    }

    filter(fn) {
        return new List(this.data.filter(fn));
    }

    map(fn) {
        return new List(this.data.map(fn));
    }

    length() {
        return this.data.length;
    }

    foldl(fn, init) {
        return this.data.reduce(fn, init);
    }

    foldr(fn, init) {
        return this.data.reduceRight(fn, init);
    }

    reverse() {
        return new List(this.data.reverse());
    }
}

const flatten = (arr, listArr) =>
      (listArr.length === 0)
          ? arr
          : flatten(arr.concat(listArr[0].values), listArr.slice(1));
