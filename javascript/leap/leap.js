/** @format */

const isDivisibleBy = mod => div => div % mod === 0;
const isDivisibleBy4 = isDivisibleBy(4);
const isDivisibleBy100 = isDivisibleBy(100);
const isDivisibleBy400 = isDivisibleBy(400);

const isLeapYear = year =>
    isDivisibleBy400(year) ||
    (isDivisibleBy4(year) && !isDivisibleBy100(year));

export default class Year {

    constructor (year) {

        this.year = year;

    }

    isLeap () {

        return isLeapYear(this.year);

    }

}
