/** @format */

/*
 * I've changed the unit tests slightly so that it does not use the
 * default export pattern.  This is considered by some as an
 * anti-pattern.  This just uses a regular export statement.
 */

const endpoints = ['UAA', 'UAG', 'UGA'];

const proteins = {
    AUG: 'Methionine',
    UUU: 'Phenylalanine',
    UUC: 'Phenylalanine',
    UUA: 'Leucine',
    UUG: 'Leucine',
    UCU: 'Serine',
    UCC: 'Serine',
    UCA: 'Serine',
    UCG: 'Serine',
    UAU: 'Tyrosine',
    UAC: 'Tyrosine',
    UGU: 'Cysteine',
    UGC: 'Cysteine',
    UGG: 'Tryptophan'
};

const rnaToCodon = (rna, codons = []) =>
    (rna === '' || endpoints.includes(rna.slice(0, 3))
        ? codons
        : rnaToCodon(rna.slice(3), codons.concat(rna.slice(0, 3))));

const codonToProtein = (codon, protein = []) => {

    if (codon.length === 0) return protein;

    const nextProtein = proteins[codon[0]];

    // eslint-disable-next-line no-eq-null
    if (nextProtein == null) throw new Error('Invalid codon');

    return codonToProtein(codon.slice(1), protein.concat(nextProtein));

};

export const translate = (rna = '') => codonToProtein(rnaToCodon(rna));
