/** @format */

export const colours = [
  'black',
  'brown',
  'red',
  'orange',
  'yellow',
  'green',
  'blue',
  'violet',
  'grey',
  'white'
];

export const value = ([x, y]) => colours.indexOf(x) * 10 + colours.indexOf(y);
