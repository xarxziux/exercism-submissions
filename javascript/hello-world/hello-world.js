/** @format */

class HelloWorld {
    hello() {
        return 'Hello, World!';
    }
}

export default HelloWorld;
