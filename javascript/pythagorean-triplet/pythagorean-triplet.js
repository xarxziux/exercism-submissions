/** @format */

export default class Triplet {
  constructor(s1, s2, s3) {
    this.s1 = s1;
    this.s2 = s2;
    this.s3 = s3;
  }
  sum() {
    return this.s1 + this.s2 + this.s3;
  }
  product() {
    return this.s1 * this.s2 * this.s3;
  }
  isPythagorean() {
    /* eslint-disable no-mixed-operators */
    return (
      this.s1 ** 2 + this.s2 ** 2 + this.s3 ** 2 ===
            Math.max(this.s1, this.s2, this.s3) ** 2 * 2
    );
  }
  where() {
    return [];
  }
}
