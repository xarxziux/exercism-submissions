/** @format */

const throwError = (msg) => {

    throw new Error(msg);

};

const tail = arr => arr.slice(1);

const hamming = (s1, s2, accum) =>
    (s1.length === 0
        ? accum
        : hamming(tail(s1), tail(s2), accum + (s1[0] !== s2[0])));

export const compute = (s1, s2) =>
    (s1.length !== s2.length
        ? throwError('DNA strands must be of equal length.')
        : hamming(s1, s2, 0));
