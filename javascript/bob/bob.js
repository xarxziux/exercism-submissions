const heyFn = msg => {

    const message = msg.trim();
    const isUpperCase =
        message === message.replace(/[a-z]/g, '') &&
        message !== message.replace(/[A-Z]/g, '');
    const isQuestion = message.slice(-1) === '?';
    const isEmpty = message === '';

    return isEmpty
        ? 'Fine. Be that way!'
        : isUpperCase && isQuestion
        ? 'Calm down, I know what I\'m doing!'
        : isUpperCase
        ? 'Whoa, chill out!'
        : isQuestion
        ? 'Sure.'
        : 'Whatever.';

};

export default class Bob {
  hey(message) {
    return heyFn (message)
  }
}
