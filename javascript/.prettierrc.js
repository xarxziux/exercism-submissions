/** @format */

module.exports = {
    printWidth: 78,
    tabWidth: 2,
    useTabs: false,
    semi: true,
    singleQuote: true,
    trailingComma: 'none',
    bracketSpacing: false,
    jsxBracketSameLine: false,
    arrowParens: 'avoid',
    requirePragma: false,
    insertPragma: true,
    proseWrap: 'never'
};
