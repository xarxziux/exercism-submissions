/** @format */

export const encode = (str, current = '', count = 0, accum = '') =>
    (str === ''
        ? accum + (count > 1 ? count : '') + current
        : str[0] === current
            ? encode(str.slice(1), current, count + 1, accum)
            : encode(
                str.slice(1),
                str[0],
                1,
                accum + (count > 1 ? count : '') + current
            ));

const splitCodeString = str => str.match(/\d*\D/g);
const splitCodeAtom = str => str.match(/^(\d*)(\D)$/).slice(1, 3);
const splitToStr = arr => (arr[0] === '' ? arr[1] : arr[1].repeat(arr[0]));

export const decode = str =>
    (str === ''
        ? ''
        : splitCodeString(str)
            .map(splitCodeAtom)
            .map(splitToStr)
            .reduce((x, y) => x + y));
