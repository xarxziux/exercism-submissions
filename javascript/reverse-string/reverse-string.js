/** @format */

/*
 * I've changed the unit tests slightly so that it does not use the
 * default export pattern.  This is considered by some as an
 * anti-pattern.  This just uses a regular export statement.
 */

export const reverseString = str =>
    str
        .split('')
        .reverse()
        .join('');
