const earthOrbitalPeriod = 31557600;

const orbitalPeriod = {
  mercury: 0.2408467,
  venus: 0.61519726,
  mars: 1.8808158,
  jupiter: 11.862615,
  saturn: 29.447498,
  uranus: 84.016846,
  neptune: 164.79132,
};

export default class SpaceAge {
  constructor (seconds) {
    this.seconds = seconds;
    this.opFn = function (op) {
      return (this.seconds / (op * earthOrbitalPeriod)).toFixed(2) - 0;
    }
  }
  onMercury () {
    return this.opFn (orbitalPeriod.mercury);
  }
  onVenus () {
    return this.opFn (orbitalPeriod.venus);
  }
  onEarth () {
    return this.opFn (1);
  }
  onMars () {
    return this.opFn (orbitalPeriod.mars);
  }
  onJupiter () {
    return this.opFn (orbitalPeriod.jupiter);
  }
  onSaturn () {
    return this.opFn (orbitalPeriod.saturn);
  }
  onUranus () {
    return this.opFn (orbitalPeriod.uranus);
  }
  onNeptune () {
    return this.opFn (orbitalPeriod.neptune);
  }
}
