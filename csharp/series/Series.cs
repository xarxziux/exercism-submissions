using System;
using System.Linq;

public static class Series
{
    public static string[] Slices(string numbers, int sliceLength)
    {
        if (sliceLength < 1 || sliceLength > numbers.Length) {
           throw new System.ArgumentException(
               "sliceLength parameter is out of range", "sliceLength");
        }

        return Enumerable
            .Range(0, (numbers.Length - sliceLength + 1))
            .Select(x => numbers.Substring(x, sliceLength))
            .ToArray();
    }
}