using System;

public class SpaceAge
{
    double earthOrbitalPeriod = 31557600.0;
    double age = 0.0;

    double GetAge(double period)
    {
        return Math.Round(this.age / (period * earthOrbitalPeriod), 2);
    }

    public SpaceAge(int seconds)
    {
        this.age = seconds;
    }

    public double OnEarth()
    {
        return this.GetAge(1);
    }

    public double OnMercury()
    {
        return this.GetAge(0.2408467);
    }

    public double OnVenus()
    {
        return this.GetAge(0.61519726);
    }

    public double OnMars()
    {
        return this.GetAge(1.8808158);
    }

    public double OnJupiter()
    {
        return this.GetAge(11.862615);
    }

    public double OnSaturn()
    {
        return this.GetAge(29.447498);
    }

    public double OnUranus()
    {
        return this.GetAge(84.016846);
    }

    public double OnNeptune()
    {
        return this.GetAge(164.79132);
    }
}