;;; -*- lexical-binding: t -*-
(require 'cl)

;; The following line is included with the template but it doesn't
;; appear to serve a purpose, and it looks like an error anyway.
;; Should it not be "(provide 'anagrams-for)"?
;; I still have no idea how ELisp work.
;;(provide 'anagram)

(defvar sorted-main-str)

(defun sort-string (str)
  "Convert a string into a sorted list"
  (sort (string-to-list (downcase str)) '<))

(defun is-anagram (base-str)
  "Test if a given string is equal to the sorted base-str"
  (let ((sorted-base-str (sort-string base-str)))
    (lambda (test-str)
      (and
       (not (string= base-str test-str))
       (equal sorted-base-str (sort-string test-str))))))

(defun anagrams-for (base-str lst)
  "Filter out all non anagrams"
  (cl-remove-if-not (is-anagram base-str) lst))
