(defun two-fer (&optional name)
  (concat "One for " (or name "you") ", one for me."))

(provide 'two-fer)
