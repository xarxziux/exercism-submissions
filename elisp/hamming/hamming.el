;;; hamming.el --- Hamming (exercism)
(provide 'hamming)

(defun hamming-distance (str1 str2)
  (if
      (/= (length str1) (length str2))
      (error "parameters must be of the same length")
    (cl-count t (cl-mapcar #'/= str1 str2))))
