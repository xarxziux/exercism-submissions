
;;; bob.el --- Bob exercise (exercism)
(require 'subr-x)

(provide 'bob)

; A "silence" is a blank string, or one that only contains whitespace
(defun is-silence? (str) (string= (string-trim str) ""))

; A "shout" is defined as a string that contains at least one
; upper case letter and no lower case letters.
(defun is-shout? (str)
  (and
   (string= str (upcase str))
   (not (string= str (downcase str)))))

; A "question" is defined as any string that ends in a "?".
(defun is-question? (str) (string-suffix-p "?" (string-trim str)))

; Now put it all together
(defun response-for (str)
  (cond
    ((is-silence? str) "Fine. Be that way!")
    ((and
      (is-shout? str)
      (is-question? str))
     "Calm down, I know what I'm doing!")
    ((is-shout? str) "Whoa, chill out!")
    ((is-question? str) "Sure.")
    (t "Whatever.")))
