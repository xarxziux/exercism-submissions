(defun leap-year-p (yr)
  (and
   (= 0 (% yr 4))
   (or
    (/= (% yr 100) 0)
    (= 0 (% yr 400)))))
