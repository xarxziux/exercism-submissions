(ns bob
    (:require [clojure.string :as str]))

; For the purposes of this exercise,
; a shout is defined as a string that:
;   1) Contains at least one upper case letter and,
;   2) Contains no lower case letters.
(defn shout? [s]
    (and
        (= s (str/upper-case s))
        (not (= s (str/lower-case s)))))

(defn question? [s]
    (str/ends-with? s "?"))

(defn response-for [s]
    (cond
        (str/blank s)
            "Fine. Be that way!"
        (and (shout? s) (question? s))
            "Calm down, I know what I'm doing!"
        (shout? s)
            "Whoa, chill out!"
        (question? s)
            "Sure."
        :else
            "Whatever."))
