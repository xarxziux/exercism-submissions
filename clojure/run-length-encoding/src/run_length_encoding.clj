(ns run-length-encoding)

(defn add-encoding [sq]
  (let [cnt (count sq)]
    (str
     (if (= cnt 1) "" (str cnt))
     (first sq))))

(defn run-length-encode [init]
  (apply
   str
   (map
    add-encoding
    (partition-by identity init))))

(defn str->count [s]
  (if s (Integer/parseInt s) 1))

(defn decode [[_ cnt chr]]
  (apply str (repeat (str->count cnt) chr)))

(defn run-length-decode [in]
  (apply
   str
   (map
    decode
    (re-seq #"(\d+)?([\w| ])" in))))
