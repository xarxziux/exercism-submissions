(ns rna-transcription)

(defn to-rna-single [dna]
  (cond
    (= dna \G) \C
    (= dna \C) \G
    (= dna \T) \A
    (= dna \A) \U
    :else (throw (AssertionError. "Invalid nucleotide"))))

(defn to-rna [dna]
  (apply str (map to-rna-single dna)))
