(ns armstrong-numbers
  (:require [clojure.string :as str]))

(defn exp [x n]
  (loop [acc 1 n n]
    (if (zero? n) acc
      (recur (* x acc) (dec n)))))

(defn sumSeqByLen [xs]
  (reduce (fn [x y] (+ x y))
    (map (fn [x] (exp x (count xs))) xs)))

(defn armstrong? [num]
  (= num
    (sumSeqByLen
      (map (fn [x] (Integer/valueOf x))
        (str/split (str num) #"")))))
