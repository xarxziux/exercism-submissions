rna_chars('G', 'C').
rna_chars('C', 'G').
rna_chars('T', 'A').
rna_chars('A', 'U').

rna_list([], []).

rna_list([H1|T1], [H2|T2]) :-
  /*
  write(H1),
  write(H2),
  write(T1),
  write(T2),
  write(''),
  */
  rna_chars(H1, H2),
  rna_list(T1, T2).

rna_transcription(X, Y) :-
  string_chars(X, X1),
  string_chars(Y, Y1),
  rna_list(X1, Y1).
