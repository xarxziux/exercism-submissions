expand([], []).
expand([X], [X]).
expand([X, Y|T], [X1|T1]) :-
  X1 =:= X + Y,
  expand([Y|T], T1).

next_level([], [1]).
next_level([H|T], [H|T1]) :-
  expand([H|T], T1).

pascal(0, []).
pascal(1, [1]).
pascal(X, Y) :-
  Z =:= X - 1,
  next_level(Y1, Y),
  pascal(Z, Y1).

%  1   2   3   4   5   6   7
% 1  3   5   7   9   11  13  7
