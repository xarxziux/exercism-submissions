real((X, _), X).
imaginary((_, Y), Y).

conjugate((X, Y), (X, Z)) :-
  Z =:= -Y.

abs((X, Y), Z) :-
  Z is sqrt(X*X + Y*Y).

add((X1, Y1), (X2, Y2), (X3, Y3)) :-
  X3 =:= X1 + X2,
  Y3 =:= Y1 + Y2.

sub((X1, Y1), (X2, Y2), (X3, Y3)) :-
  X3 =:= X1 - X2,
  Y3 =:= Y1 - Y2.

mul((A, B), (C, D), (E, F)) :-
  E =:= A * C - B * D,
  F =:= B * C + A * D.

div((A, B), (C, D), (E, F)) :-
  E =:= (A * C + B * D)/(C^2 + D^2),
  F =:= (B * C - A * D)/(C^2 + D^2).
