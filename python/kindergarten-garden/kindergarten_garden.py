DEFAULT_STUDENTS = [
    "Alice", "Bob", "Charlie", "David",
    "Eve", "Fred", "Ginny", "Harriet",
    "Ileana", "Joseph", "Kincaid", "Larry"
]

CHAR_TO_PLANT = {
    "C": "Clover",
    "G": "Grass",
    "R": "Radishes",
    "V": "Violets"
}

class Garden(object):
    def __init__(self, diagram, students = None):
        self.students = sorted (students or DEFAULT_STUDENTS)
        self._group_plants(diagram.splitlines())

    def plants (self, s):
        return self.plant.get(s)

    def _group_plants(self, s):
        self.plant = {}
        for i in range (len(s[0]) // 2):
            self.plant[self.students[i]] = (
                    list(map(CHAR_TO_PLANT.get,
                    s[0][i*2 : i*2 + 2] + s[1][i*2 : i*2 + 2])))
