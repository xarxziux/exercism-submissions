allergy_list = [
    "eggs",
    "peanuts",
    "shellfish",
    "strawberries",
    "tomatoes",
    "chocolate",
    "pollen",
    "cats"]

class Allergies(object):

    def __init__(self, score):
        self.allergy_set = set()
        for i in range (8):
            if score & (1 << i) != 0:
                self.allergy_set.add (allergy_list[i])

    def is_allergic_to(self, item):
        return item in self.allergy_set

    @property
    def lst(self):
        return list(self.allergy_set)
