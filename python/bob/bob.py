def hey(phrase):
    clean_phrase = phrase.strip()

    if not clean_phrase:
        return 'Fine. Be that way!'

    upper = clean_phrase.isupper()
    question = clean_phrase.endswith('?')

    if upper and question:
        return 'Calm down, I know what I\'m doing!'

    if upper:
        return 'Whoa, chill out!'

    if question:
        return 'Sure.'

    return 'Whatever.'
