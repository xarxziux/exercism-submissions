from string import Template

def two_fer(name="you"):
    return Template("One for $n, one for me.").substitute(n=name)
