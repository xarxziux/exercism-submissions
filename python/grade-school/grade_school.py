from collections import defaultdict

class School(object):
    def __init__(self):
        self._students = defaultdict(lambda: [])

    def add_student(self, name, grade):
        self._students[grade] = sorted(self._students[grade] + [name])

    def roster(self):
        return ([s
                for g in sorted(self._students.keys())
                for s in self._students[g]])

    def grade(self, grade_number):
        return self._students[grade_number]
