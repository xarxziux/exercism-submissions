def abbreviate(phrase):
    return ''.join(
        char2.upper()
        for char1, char2 in zip(' ' + phrase, phrase)
        if char1 in' -_'
        and char2.isalpha())
