import re

def is_pangram(sentence):
    return len(set(re.findall("[a-z]", sentence.lower()))) == 26
