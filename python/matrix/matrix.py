class Matrix(object):
    _grid = []

    def __init__(self, matrix_string):
        self._grid = list(map(lambda x: list(map(int, x)),
                              map(lambda s: s.split(" "),
                                  matrix_string.split("\n"))))

    def row(self, index):
        return self._grid[index - 1]

    def column(self, index):
        return [self._grid[x][index - 1] for x in range(len(self._grid))]
