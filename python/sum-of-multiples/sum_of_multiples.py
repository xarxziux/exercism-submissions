from functools import reduce

def seq_range (limit):
    return lambda base: range (base, limit, base)

def comb_sets (s1, s2):
    return set(s1).union(set (s2))

def sum_of_multiples (limit, multiples):
    return sum (reduce (comb_sets, map (seq_range (limit), multiples), []))
