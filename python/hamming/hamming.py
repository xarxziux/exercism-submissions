def hamming (s1, s2):
    return sum(l != r for l, r in zip(s1, s2))

def distance(strand_a, strand_b):
    if len(strand_a) != len(strand_b):
        raise ValueError ('Cannot compare strings with different lengths')

    return hamming(strand_a, strand_b)
