from string import Template

msg_start = Template("Your latest score was $latest. That's ")
not_best = Template("$diff short of ")
msg_end =  "your personal best!"

def get_report(latest, highest):
    if (latest >= highest):
        return msg_start.substitute(latest = latest) + msg_end

    return (msg_start.substitute(latest = latest) +
            not_best.substitute(diff = highest - latest) +
            msg_end)

def top_three(arr):
    return sorted(arr, reverse = True)[0:3]

class HighScores(object):
    def __init__(self, arr):
        self.scores = arr
        self._latest = arr[-1]
        self._personal_best = max(arr)
        self._personal_top = top_three(arr)
        self._report = get_report(self._latest, self._personal_best)

    def latest(self):
        return self._latest

    def personal_best(self):
        return self._personal_best

    def personal_top(self):
        return self._personal_top

    def report(self):
        return self._report
