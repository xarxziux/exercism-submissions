(define-module (leap-year)
  #:export (leap-year?))

(define (isDivisibleBy num div)
  (= 0 (modulo num div))
)

(define (leap-year? year)
  (or
    (isDivisibleBy year 400)
    (and
      (not (isDivisibleBy year 100))
      (isDivisibleBy year 4)
    )
  )
)
