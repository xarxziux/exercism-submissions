(define-module (raindrops)
  #:export (convert))

(define (pling n)
  (if (= 0 (modulo n 3)) "Pling" ""))

(define (plang n)
  (if (= 0 (modulo n 5)) "Plang" ""))

(define (plong n)
  (if (= 0 (modulo n 7)) "Plong" ""))

(define (ppp n)
  (string-append (pling n) (plang n) (plong n)))

(define (convert n)
  (define pn (ppp n))
  (if (string=? "" pn) (number->string n) pn))
