(define-module (hamming)
  #:export (hamming-distance))

(define (same_elem e1 e2) (if (char=? e1 e2) 0 1))

(define (distance str1 str2 accum)
  (if
    (string-null? str1)
    accum
    (distance
      (substring str1 1)
      (substring str2 1)
      (+
        accum
        (same_elem
          (string-ref str1 0)
          (string-ref str2 0)
        )
      )
    )
  )
)

(define (hamming-distance str1 str2)
  (if
    (= (string-length str1) (string-length str1))
    (distance str1 str2 0)
    (raise "Invalid input")
  )
)
