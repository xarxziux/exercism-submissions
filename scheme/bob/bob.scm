(define-module (bob)
  #:export (response-for))

(define (isShout? s)
    (and
        (string-any char-upper-case? s)
        (not (string-any char-lower-case? s))))

(define (isQuestion? s)
    (equal?
        (string-take-right s 1)
        "?"))

(define (response-for x)
    (cond
        ((equal? (string-trim-both x) "") "Fine. Be that way!")
        ; Is this condition missing from the spec?
        ; ((and (isShout? x) (isQuestion? x)) "Calm down, I know what I'm doing!")
        ((isShout? x) "Whoa, chill out!")
        ((isQuestion? x) "Sure.")
        (else "Whatever.")))
