module Bob

open System.Text.RegularExpressions

let isShout (str:string):bool =
  Regex.Match(str, "[A-Z]").Success
  && not (Regex.Match(str, "[a-z]").Success)

let response (input:string):string =
  let tstr:string = input.Trim()
  let s:bool = isShout tstr
  let q:bool = tstr.EndsWith "?"

  if String.length tstr = 0 then "Fine. Be that way!"
  elif s && q then "Calm down, I know what I\'m doing!"
  elif s then "Whoa, chill out!"
  elif q then "Sure."
  else "Whatever."
