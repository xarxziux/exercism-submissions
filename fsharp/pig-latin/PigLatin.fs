﻿module PigLatin

let vowels: Set<char> = Set.ofList ['a'; 'e'; 'i'; 'o'; 'u']

let vowelSounds: Set<string> = Set.ofList ["xr"; "yt"]

let findFirstVowel (s: string): int =
  let rec ffvInt (s: string) (a:int): int =
    if Set.contains s.[0] vowels then a
    else if a = 0 && Set.contains s.[0 .. 1] vowelSounds then a
    else if a <> 0 && s.[0] = 'y' then a
    else if s.[ .. 1] = "qu" then a + 2
    else ffvInt s.[1 .. ] (a + 1)
  ffvInt s 0

let toPigLatin (s: string): string =
  let x: int = findFirstVowel s
  match x with
    | 0 -> s + "ay"
    | _ -> s.[x .. ] + s.[0 .. (x - 1)] + "ay"

let translate (input: string) =
  input.Split ' '
    |> Array.map toPigLatin
    |> String.concat " "
