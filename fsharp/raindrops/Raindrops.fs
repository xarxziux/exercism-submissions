module Raindrops

(*
 * This submission is designed to be generic.  The raindrop List
 * is separated out to it's own value instead of being hard-coded
 * inside the main functions.  This should allow the list to be
 * easily extended by simply adding more match tuples to the
 * raindrops list.
 *)

type Drop = int * string

let raindrops: Drop list =
    [
        (3, "Pling");
        (5, "Plang");
        (7, "Plong")
    ]

let getRaindrops (n: int): string =
    List.filter (fun (x, _) -> n % x = 0) raindrops
    |> List.map snd
    |> String.concat ""

let convert (n: int): string =
    match (getRaindrops n) with
    | "" -> n.ToString()
    | x -> x
