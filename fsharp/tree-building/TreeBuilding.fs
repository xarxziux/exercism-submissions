module TreeBuilding

(*
 * This submission is passing but not quite finished yet.  I've
 * separated and flattened the validation logic and added type
 * definitions everywhere so the code is generally a little neater.
 *
 * This is an improvement, but I haven't touched the main logic of
 * the algorithm yet (now contained in the buildVerifiedTree()
 * function).  I'm still not entirely sure what's going on in there
 * so I want to have another look at that, but it's taken me long
 * enough to get here.
 *)

type Record = { RecordId: int; ParentId: int }

type Tree =
  | Branch of int * Tree list
  | Leaf of int

let recordId (t: Tree): int =
  match t with
  | Branch (id, c) -> id
  | Leaf id -> id

let isBranch (t: Tree): bool =
  match t with
  | Branch (id, c) -> true
  | Leaf id -> false

let children (t: Tree): Tree list =
  match t with
  | Branch (id, c) -> c
  | Leaf id -> []

let isHead (r: Record): bool =
  r.ParentId = 0 && r.RecordId = 0

let rec isValidChildren (r: Record list) (i: int): bool =
  match r with
  | [] -> true
  | h :: t ->
    h.RecordId = i &&
    h.ParentId < h.RecordId &&
    isValidChildren t (i + 1)

let verifyRecords (r: Record list): bool =
  match r with
  | [] -> false
  | h :: t ->
    isHead h && isValidChildren t 1

let buildVerifiedTree (r: Record list): Tree =
  let leafs: (int * int) list =
    (-1, 0) ::
    (List.map (fun x -> (x.ParentId, x.RecordId)) r |> List.tail)

  let grouped: (int * int list) list =
    leafs
    |> List.groupBy fst
    |> List.map (fun (x, y) -> (x, List.map snd y))

  let parens: int list = List.map fst grouped

  let map: Map<int, int list> = grouped |> Map.ofSeq

  let rec helper (key: int) =
    if Map.containsKey key map then
      Branch (key, List.map (fun i -> helper i) (Map.find key map))
    else
      Leaf key

  helper 0

let buildTree (r: Record list): Tree =
    let r' = List.sortBy (fun x -> x.RecordId) r

    match verifyRecords r' with
    | false -> failwith "Validation check failed"
    | true -> buildVerifiedTree r'
