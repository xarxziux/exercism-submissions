module Acronym

open System

let isHead (x: char, y: char): bool =
    (Char.IsLower x && Char.IsUpper y) ||
        (String.exists ((=) x) " -" && Char.IsLetter y)

let abbreviate (x: string) =
    Seq.zip (String.concat "" [" "; x]) x
        |> Seq.filter isHead
        |> Seq.map snd
        |> Seq.map Char.ToUpper
        |> Array.ofSeq
        |> String
