module QueenAttack

let isValidRank (n: int): bool =
  n >= 0 && n < 8

let create (x: int, y: int): bool =
  isValidRank x && isValidRank y

let canAttack (q1: int * int) (q2: int * int) =
  (fst q1 = fst q2) ||
    (snd q1 = snd q2) ||
    (abs (fst q2 - fst q1)) = (abs (snd q2 - snd q1))
