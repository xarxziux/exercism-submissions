module Clock

type Minutes = Minutes of int

// There doesn't appear to be a proper "modulus" function in the
// standard library.  Is this true?
let intToMinutes (x: int): Minutes =
    match x >= 0 with
    | true -> Minutes (x % 1440)
    | false -> Minutes (1440 - (-x) % 1440)

let create (x: int) (y: int): Minutes =
    intToMinutes (x * 60 + y)

let add (x: int) (Minutes y): Minutes =
    intToMinutes (x + y)

let subtract (x: int) (Minutes y): Minutes =
    intToMinutes (y - x)

let display (Minutes x): string =
    (x / 60).ToString().PadLeft (2, '0')
        + ":"
        + (x % 60).ToString().PadLeft(2, '0')
