module Pangram

let isPangram (input: string): bool =
    input.ToCharArray()
    |> Array.map System.Char.ToLower
    |> Array.filter System.Char.IsLower
    |> Array.distinct
    |> Array.length
    |> (=) 26
