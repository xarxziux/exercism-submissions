module RnaTranscription

let toRnaChar (c: char): char =
    match c with
    | 'G' -> 'C'
    | 'C' -> 'G'
    | 'T' -> 'A'
    | 'A' -> 'U'
    | _ -> failwith "No unit tests for this case"

let toRna (dna: string): string =
    String.map toRnaChar dna
