module ArmstrongNumbers

let sumSeqByLen (xs: int seq): int =
    Seq.map (fun x -> Seq.length xs |> pown x) xs
        |> Seq.sum

let charToInt (x: char): int =
    System.Char.GetNumericValue x
        |> int

let isArmstrongNumber (x: int): bool =
    x.ToString()
        |> Seq.map charToInt
        |> sumSeqByLen
        |> (=) x
