module Sieve

let isFactorOf (f: int) (b: int): bool =
    f > 1 && f <> b && b % f = 0

let isNotFactorOf (f: int) (b: int): bool =
    isFactorOf f b |> not

let rec sieve (accum: int list, xs: int list): int list =
    match xs with
    | [] -> accum
    | x::xs -> sieve (x :: accum, List.filter (isNotFactorOf x) xs)

let primes (x: int): int list =
    match x < 2  with
    | true -> []
    | false -> sieve ([], [2 .. x]) |> List.rev
