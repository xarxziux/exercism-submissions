module BeerSong

// Translation of R submission

(*
 * This seems to work!
 *)

let beer = " of beer"
let onTheWall = " on the wall"
let takeOne = " down and pass it around, "
let moreBeer =
    "Go to the store and buy some more, 99 bottles of beer on the wall."

let strBottle (n: int): string =
    match n with
    | 1 -> "bottle"
    | _ -> "bottles"

let strCount (n: int): string =
    match n with
    | 0 -> "no more "
    | _ -> n.ToString() + " "

let strPronoun (n: int): string =
    match n with
    | 1 -> "Take it"
    | _ -> "Take one"

let capitalise (s: string): string =
    match s.[0..6] with
    | "no more" -> "N" + s.[1..]
    | _ -> s

let verse (n: int): string list =
    let (bob: string) =
        String.concat "" [
            strCount n;
            strBottle n;
            beer
        ]
    let firstLine =
        String.concat "" [
            capitalise bob;
            onTheWall;
            ", ";
            bob;
            "."
        ]
    let (lastLine: string) =
        match n with
        | 0 -> moreBeer
        | _ -> String.concat "" [
            strPronoun n;
            takeOne;
            strCount (n - 1);
            strBottle (n - 1);
            beer;
            onTheWall;
            "."
        ]
    [firstLine; lastLine]

let recite (s: int) (t: int): string list =
    [s .. -1 .. (s - t + 1)]
    |> List.map verse
    |> List.reduce (fun x y -> x @ "" :: y)
