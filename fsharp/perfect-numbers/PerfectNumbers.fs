module PerfectNumbers

type Classification = Perfect | Abundant | Deficient

let getClassification (num: int) (result: int): Classification =
    match (num - result) with
    | 0 -> Perfect
    | a when a > 0 -> Deficient
    | a when a < 0 -> Abundant

let classify (n: int): Classification option =
    match n with
    | x when x <= 0 -> None
    | _ ->
        seq {for x in 1..(n - 1) do if n % x = 0 then yield x}
        |> Seq.sum
        |> getClassification n
        |> Some
