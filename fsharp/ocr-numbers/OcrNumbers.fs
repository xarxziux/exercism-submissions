module OcrNumbers

let rec splitInThree (s: string): string list =
  match s with
  | "" -> []
  | _ -> s.[0 .. 2] :: splitInThree s.[3 .. ]

let rec splitInFour (xs: string list): string list list=
  match xs with
  | [] -> []
  | _ -> xs.[0 .. 3] :: splitInFour xs.[4 .. ]

let parseLine0 (s: string): int =
  match s with
  | "   " -> 1
  | " _ " -> 2
  | _ -> 0

let parseLine1 (s: string): int =
  match s with
  | "  |" -> 10
  | " _|" -> 20
  | "|_ " -> 30
  | "|_|" -> 40
  | "| |" -> 50
  | _ -> 0

let parseLine2 (s: string): int =
  match s with
  | "  |" -> 100
  | "|_ " -> 200
  | " _|" -> 300
  | "|_|" -> 400
  | _ -> 0

let parseLine3 (s: string): int =
  match s with
  | "   " -> 1000
  | _ -> 0

let sumToChar (x: int): char =
  match x with
  | 1111 -> '1'
  | 1222 -> '2'
  | 1322 -> '3'
  | 1141 -> '4'
  | 1332 -> '5'
  | 1432 -> '6'
  | 1112 -> '7'
  | 1442 -> '8'
  | 1342 -> '9'
  | 1452 -> '0'
  | _ -> '?'

let parseAll (xs: string list): string =
  let xss = List.map splitInThree xs
  List.map parseLine0 xss.[0]
    |> List.map2 (+) (List.map parseLine1 xss.[1])
    |> List.map2 (+) (List.map parseLine2 xss.[2])
    |> List.map2 (+) (List.map parseLine3 xss.[3])
    |> List.map sumToChar
    |> Array.ofList
    |> System.String

let convert (xs: string list): string option =
  if List.length xs % 4 <> 0 then None
  elif List.exists (fun x -> String.length x % 3 <> 0) xs then None
  else xs
    |> splitInFour
    |> List.map parseAll
    |> String.concat ","
    |> Some
 