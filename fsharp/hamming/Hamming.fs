module Hamming

// Slight alteration based on a community solution

let distance (strand1: string) (strand2: string): int option =
  if strand1.Length <> strand2.Length then None
  else Seq.map2 (fun s1 s2 -> if s1 = s2 then 0 else 1) strand1 strand2
    |> Seq.sum
    |> Some
