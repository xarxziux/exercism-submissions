module RobotSimulator

type Direction = North | East | South | West

type Position = int * int

type Robot = { direction: Direction; position: Position }

let create (d: Direction) (p: Position): Robot =
  {direction = d; position = p}

let turnLeft ({direction = d; position = p}): Robot =
  match d with
  | North -> {direction = West; position = p}
  | West -> {direction = South; position = p}
  | South -> {direction = East; position = p}
  | East -> {direction = North; position = p}

let turnRight ({direction = d; position = p}): Robot =
  match d with
  | North -> {direction = East; position = p}
  | East -> {direction = South; position = p}
  | South -> {direction = West; position = p}
  | West -> {direction = North; position = p}

let advance ({direction = d; position = p}): Robot =
  match d with
  | North -> {direction = d; position = Position (fst p, snd p + 1)}
  | East -> {direction = d; position = Position (fst p + 1, snd p)}
  | South -> {direction = d; position = Position (fst p, snd p - 1)}
  | West -> {direction = d; position = Position (fst p - 1, snd p)}

let charToFun (c: char) =
  match c with
  | 'L' -> turnLeft
  | 'R' -> turnRight
  | 'A' -> advance
  | _ -> failwith "Invalid input"

let instructions (instructions': string) (robot: Robot) =
  Seq.map charToFun instructions'
    |> Seq.fold (fun a f -> f a) robot
