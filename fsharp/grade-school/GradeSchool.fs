module GradeSchool

type School = Map<int, string list>

let empty: School = Map.empty

let add (student: string) (grade: int) (school: School): School =
    match Map.tryFind grade school with
    | None -> Map.add grade [student] school
    | Some x -> Map.add grade (student :: x |> List.sort) school

let roster (school: School): (int * string list) list =
    Map.toList school

let grade (number: int) (school: School): string list =
    match Map.tryFind number school with
    | None -> []
    | Some x -> x
