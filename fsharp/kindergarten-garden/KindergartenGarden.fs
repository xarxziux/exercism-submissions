module KindergartenGarden

type Plant = Clover | Grass | Radishes | Violets

let charToPlant (c: char): Plant =
  match c with
  | 'C' -> Clover
  | 'G' -> Grass
  | 'R' -> Radishes
  | 'V' -> Violets
  | _ -> failwith "Match failure"

let plants (diagram: string) (student: string): Plant list =
  let a = diagram.Split('\n')
  let i = int student.[0] - 65 |> (*) 2
  a.[0].[i .. i + 1] + a.[1].[i .. i + 1]
    |> Seq.map charToPlant
    |> Seq.toList
