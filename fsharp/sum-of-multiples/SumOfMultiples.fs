module SumOfMultiples

let isDivisibleBy (num: int) (div: int): bool =
  num % div = 0

let isDivList (arr: int list) (num: int): bool =
  List.exists (isDivisibleBy num) arr

let sum (numbers: int list) (upperBound: int): int =
  Seq.sum <| seq {for n in 1..(upperBound-1) do if isDivList numbers n then yield n}
