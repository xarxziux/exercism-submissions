module DifferenceOfSquares

let squareOfSum (number: int): int =
    pown (seq{1..number} |> Seq.sum) 2

let sumOfSquares (number: int): int =
    seq {for i in 1..number -> i * i} |> Seq.sum

let differenceOfSquares (number: int): int =
    squareOfSum number - sumOfSquares number
