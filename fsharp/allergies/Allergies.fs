module Allergies

type Allergen =
    | Eggs = 1
    | Peanuts = 2
    | Shellfish = 4
    | Strawberries = 8
    | Tomatoes = 16
    | Chocolate = 32
    | Pollen = 64
    | Cats = 128

let allAllergies: Allergen list =
    [
        Allergen.Eggs;
        Allergen.Peanuts;
        Allergen.Shellfish;
        Allergen.Strawberries;
        Allergen.Tomatoes;
        Allergen.Chocolate;
        Allergen.Pollen;
        Allergen.Cats;
    ]

let allergicTo (x: int) (a: Allergen): bool =
    int a |> (&&&) x |> (<>) 0

let list (x: int): Allergen list =
    List.filter (fun a -> allergicTo x a) allAllergies
