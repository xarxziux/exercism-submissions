module SpaceAge

type Planet = Planet of float

let Mercury = Planet 0.2408467
let Venus = Planet 0.61519726
let Earth = Planet 1.0
let Mars = Planet 1.8808158
let Jupiter = Planet 11.862615
let Saturn = Planet 29.447498
let Uranus = Planet 84.016846
let Neptune = Planet 164.79132

let earthOrbitalPeriod = 31557600.0

let age (Planet planet) (seconds: int64): float =
    float seconds / (planet * earthOrbitalPeriod)
