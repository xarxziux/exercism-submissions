module CollatzConjecture

let rec collatz (current: int) (step_count: int): int =
  if current = 1 then step_count
  elif current % 2 = 0 then collatz (current / 2) (step_count + 1)
  else collatz (current * 3 + 1) (step_count + 1)

let steps (number: int): int option =
  if number <= 0 then None
  else Some <| collatz number 0
