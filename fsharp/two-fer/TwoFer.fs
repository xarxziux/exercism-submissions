module TwoFer

let twoFer (input: string option): string =
  match input with Some i -> i | _ -> "you"
  |> sprintf "One for %s, one for me."
