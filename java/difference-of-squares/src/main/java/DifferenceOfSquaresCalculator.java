class DifferenceOfSquaresCalculator
{
   int computeSquareOfSumTo(final int input)
   {
      return (int)(Math.pow(input, 4) +
         2 * (Math.pow(input, 3)) +
         Math.pow(input, 2)) / 4;
   }

   int computeSumOfSquaresTo(final int input)
   {
      return (input * (input + 1) * (2 * input + 1)) / 6;
   }

   int computeDifferenceOfSquares(final int input)
   {
      return computeSquareOfSumTo(input) - computeSumOfSquaresTo(input);
   }
}
