import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

class Gigasecond {
    private LocalDateTime dt;

    public Gigasecond(LocalDate moment) {
        this(moment.atStartOfDay());
    }

    public Gigasecond(LocalDateTime moment) {
        dt = moment.plus((long)1_000_000_000, ChronoUnit.SECONDS);
    }

    public LocalDateTime getDateTime() {
        return dt;
    }

}
