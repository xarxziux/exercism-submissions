import java.util.HashMap;

class Scrabble
{
   static final HashMap<Character, Integer> points = new HashMap<>();
   Integer score;

   {
      points.put('a', 1);
      points.put('e', 1);
      points.put('i', 1);
      points.put('o', 1);
      points.put('u', 1);
      points.put('l', 1);
      points.put('n', 1);
      points.put('r', 1);
      points.put('s', 1);
      points.put('t', 1);
      points.put('d', 2);
      points.put('g', 2);
      points.put('b', 3);
      points.put('c', 3);
      points.put('m', 3);
      points.put('p', 3);
      points.put('f', 4);
      points.put('h', 4);
      points.put('v', 4);
      points.put('w', 4);
      points.put('y', 4);
      points.put('k', 5);
      points.put('j', 8);
      points.put('x', 8);
      points.put('q', 10);
      points.put('z', 10);
   }

   Scrabble(String word)
   {
      score = word.chars().map(this::getPoints).sum();
   }

   private int getPoints(int i)
   {
      return points.getOrDefault(Character.toLowerCase((char)i), 0);
   }

   int getScore()
   {
      return score;
   }
}
