import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;

class HandshakeCalculator {
    List<Signal> calculateHandshake(int number)
    {
       final Signal[] signals = Signal.values();
       final List<Signal> answer = new ArrayList<>();
       final IntPredicate isBitSet = getIsBitSet(number);
       final int elems = signals.length;
       final boolean toBeReversed = isBitSet.test(elems);

       for (Integer i = 0; i < elems; i++) {
           final int j = toBeReversed ? elems - i - 1 : i;

           if (isBitSet.test(j)) {
               answer.add(signals[j]);
           }
       }

       return answer;
    }

    static public IntPredicate getIsBitSet(int code){
        return index -> (code & (1 << index)) != 0;
    }
}
