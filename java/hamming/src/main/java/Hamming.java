class Hamming {

    private int hammingDistance = 0;

    Hamming(String leftStrand, String rightStrand)
        throws IllegalArgumentException {
        final int leftLength = leftStrand.length();
        final int rightLength = rightStrand.length();

        if(leftLength == 0 && rightLength == 0) {
            hammingDistance = 0;
            return;
        }

        if(leftLength == 0) {
            throw new IllegalArgumentException
                ("left strand must not be empty.");
        }

        if(rightLength == 0) {
            throw new IllegalArgumentException
                ("right strand must not be empty.");
        }

        if(leftLength != rightLength) {
            throw new IllegalArgumentException
                ("leftStrand and rightStrand must be of equal length.");
        }

        for(int i = 0; i < leftLength; i++) {
            if(leftStrand.charAt(i) != rightStrand.charAt(i)) {
                hammingDistance++;
            }
        }
    }

    int getHammingDistance() {
        return hammingDistance;
    }
}
