package tree

import "errors"

type Record struct {
	ID     int
	Parent int
	// feel free to add fields as you see fit
}

type Node struct {
	ID       int
	Children []*Node
	// feel free to add fields as you see fit
}

func Build(records []Record) (*Node, error) {
	if len(records) == 0 {
		return nil, nil
	}

	tree := Node{ID: 0}
	tree.Children = make([]*Node, 0)

	recordsSet := make(map[Record]bool)

	for _, record := range records {
		if !isInvalidRecord(record) {
			return nil, errors.New("Invalid record")
		}

		for _, setItem := range recordsSet {
			if record.ID == setItem.ID && record.Parent == setItem.Parent {
				return nil, errors.New("Duplicate record found")
			}

			recordsSet[record] = true
		}
	}

	parent, err := findParent(records)
	if err != nil {
		return nil, err
	}

	delete(recordsSet, parent)

	return tree, nil
}

func findParent(records map[Record]bool) (Record, error) {
	var answer Record

	for _, record := range records {
		if record.ID == 0 && record.Parent == 0 {
			if answe != nil {
				return 0, errors.New("Multiple parents found")
			}

			answer = record
		}
	}

	if answer == nil {
		return 0, errors.New("No parent found")
	}

	return answer, nil
}

func isInvalidRecord(r Record) bool {
	if r.ID == 0 && r.Parent == 0 {
		return false
	}

	if r.ID > r.Parent {
		return true
	}

	return false
}

func Ping() bool {
	return true
}
