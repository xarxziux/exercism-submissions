package scrabble

func char_to_score(c rune) int {
	scrabble_scores := [26]int{
		1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3,
		1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10,
	}
	a := int(c)
	switch {
	case a >= 65 && a <= 90:
		return scrabble_scores[a-65]
	case a >= 97 && a <= 122:
		return scrabble_scores[a-97]
	default:
		return 0
	}
}

func Score(s string) int {
	score := 0
	for _, c := range s {
		score += char_to_score(c)
	}
	return score
}
