package letter

import "sync"

// FreqMap records the frequency of each rune in a given text.
type FreqMap map[rune]int

// Frequency counts the frequency of each rune in a given text and returns this
// data as a FreqMap.
func Frequency(s string) FreqMap {
	m := FreqMap{}

	for _, r := range s {
		m[r]++
	}

	return m
}

// ConcurrentFrequency - This function takes an array of string and returns
// the combined letter frequency of all string
func ConcurrentFrequency(arr []string) FreqMap {
	answer := make(FreqMap)
	l := len(arr)
	ch := make(chan FreqMap, l)
	wg := sync.WaitGroup{}

	wg.Add(l)

	for _, str := range arr {
		go func(s string) {
			ch <- Frequency(s)
			wg.Done()
		}(str)
	}

	wg.Wait()
	close(ch)

	for result := range ch {
		for key, val := range result {
			answer[key] += val
		}
	}

	return answer
}
