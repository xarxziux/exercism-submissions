package clock

import "fmt"

func mod(num, base int) int {
	return ((num % base) + base) % base
}

// Clock struct consisting of hour and minute fields.
type Clock struct{ hours, mins int }

// New creates a new clock instance, limited to a new positive
// value between 0 and 24 hours.
func New(h, m int) Clock {
	pm := m

	if m < 0 {
		pm = mod(m, 24*60)
	}

	c := new(Clock)
	c.hours = mod(h+(pm/60), 24)
	c.mins = mod(pm, 60)
	return *c
}

// Add a given number of minutes to a Clock struct.
func (c Clock) Add(diff int) Clock {
	return New(c.hours, c.mins+diff)
}

// Subtract a given number of minutes from a Clock struct.
func (c Clock) Subtract(diff int) Clock {
	return New(c.hours, c.mins-diff)
}

// Clock method for converting a Clock instance to a string
// value in the HH:MM format.
func (c Clock) String() string {
	return fmt.Sprintf(
		"%02d:%02d",
		c.hours,
		c.mins,
	)
}
