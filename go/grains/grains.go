package grains

import "errors"

// Return the total number of grains on the chess board.
func Total() uint64 {
	return uint64(1<<64 - 1)
}

// Return the number of grains on each square.
func Square(n int) (uint64, error) {
	if n < 1 || n > 64 {
		return 0, errors.New("Invalid index")
	}

	return 1 << uint64(n-1), nil
}
