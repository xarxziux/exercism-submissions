package isogram

import (
	"strings"
	"unicode"
)

func IsIsogram(s string) bool {

	lower := strings.ToLower(s)
	chars := make(map[rune]bool)

	for _, c := range lower {
		if chars[c] {
			return false
		}

		if unicode.IsLetter(c) {
			chars[c] = true
		}
	}
	return true
}
