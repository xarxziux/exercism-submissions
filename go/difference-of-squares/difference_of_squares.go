package diffsquares

import "math"

func SquareOfSum(x int) int {
	return int(
		math.Pow(float64(x), 4.0)+
			2*(math.Pow(float64(x), 3.0))+
			math.Pow(float64(x), 2.0)) / 4
}

func SumOfSquares(x int) int {
	return (x * (x + 1) * (2*x + 1)) / 6
}

func Difference(x int) int {
	return SquareOfSum(x) - SumOfSquares(x)
}
