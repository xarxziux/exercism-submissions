package luhn

import "strings"

// Valid tests the input string against the Luhn algorithm
func Valid(str string) bool {
	var cleanStr = strings.Trim(str, " ")

	if len(cleanStr) < 2 {
		return false
	}

	var double bool
	var accum int

	for i := len(cleanStr) - 1; i >= 0; i-- {
		var n = cleanStr[i]

		if n == ' ' {
			continue
		}

		v := int(n - '0')

		if v < 0 || v > 9 {
			return false
		}

		if double {
			v *= 2
			if v > 9 {
				v -= 9
			}
		}

		accum += v

		double = !double
	}

	return accum%10 == 0
}
