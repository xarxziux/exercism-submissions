package raindrops

import (
	"strconv"
)

func Convert(x int) string {
	ans := ""

	if x%3 == 0 {
		ans = ans + "Pling"
	}
	if x%5 == 0 {
		ans = ans + "Plang"
	}
	if x%7 == 0 {
		ans = ans + "Plong"
	}

	if ans == "" {
		return strconv.Itoa(x)
	}

	return ans
}
