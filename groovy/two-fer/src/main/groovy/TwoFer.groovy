class TwoFer {
    static String twoFer(String name = "you") {
        "One for ${name ?: "you"}, one for me."
    }
}
