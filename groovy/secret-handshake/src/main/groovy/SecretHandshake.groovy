class SecretHandshake {
    static final List<String> events = [
            "wink",
            "double blink",
            "close your eyes",
            "jump"
    ]

    static final int eventsSize = events.size()

    static List<String> commands(int n) {
        getIndices(isBitSet(n, eventsSize))
                .findResults{
                    isBitSet(n, it) ? events[it] : null
                }
    }

    static List<Integer> getIndices(Boolean isReversed) {
        isReversed ? (eventsSize - 1)..0 : 0..<eventsSize
    }

    static Boolean isBitSet(int code, int index) {
        (code & 2**index) > 0
    }
}
