class RotationalCipher {
    private final int key
    static final int LOWER_A = 97
    static final int LOWER_Z = 122
    static final int UPPER_A = 65
    static final int UPPER_Z = 90

    RotationalCipher(int key) {
        this.key = ((key % 26) + 26) % 26
    }

    String rotate(String cipherText) {
        cipherText.toCharArray().collect{rotateChar(it as int)}.join()
    }

    private Character rotateChar(int chr) {
        chr >= LOWER_A && chr <= LOWER_Z
            ? addKey(chr, LOWER_A)
            : chr >= UPPER_A && chr <= UPPER_Z
            ? addKey(chr, UPPER_A)
            : (char)chr
    }

    private Character addKey(int chr, int base) {
        (((chr - base + key) % 26) + base) as char
    }
}
