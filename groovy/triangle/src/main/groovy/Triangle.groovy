class Triangle {
    static boolean isEquilateral(Number a, Number b, Number c)
    {
        isValidTriangle(a, b, c) && a == b && a == c
    }

    static boolean isIsosceles(Number a, Number b, Number c)
    {
        isValidTriangle(a, b, c) && (a == b || a == c || b == c)
    }

    static boolean isScalene(Number a, Number b, Number c)
    {
        isValidTriangle(a, b, c) && !Triangle.isIsosceles(a, b, c)
    }

    static boolean isValidTriangle(Number a, Number b, Number c)
    {
        Math.min(a, Math.min(b, c)) > 0 && (a + b + c > 2 * Math.max(a, Math.max(b, c)))
    }
}
