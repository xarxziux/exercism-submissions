class Matrix {
    private final int[][] rowGrid
    private final int[][] colGrid

    private final String EOL = "\n"
    private final String SPACE = " "

    Matrix(String str) {
        rowGrid = str
                .split(EOL)
                .collect {
                    it.split(SPACE).collect {it as Integer}
                }

        colGrid = transpose(rowGrid)
    }

    int[] row(int n) {
        rowGrid[n]
    }

    int[] column(int n) {
        colGrid[n]
    }

    private int[][] transpose(int[][] grid){
        def tGrid = new int[grid[0].length][grid.length]

        grid.eachWithIndex{ int[] row, int i ->
            row.eachWithIndex{ int cell, int j ->
                tGrid[j][i] = cell
            }
        }

        return tGrid
    }
}
