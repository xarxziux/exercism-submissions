import java.time.LocalDate
import java.time.LocalDateTime

class Gigasecond {
    static final long billion = 1_000_000_000

    static add(LocalDate moment) {
        add(moment.atStartOfDay())
    }

    static add(LocalDateTime moment) {
        moment.plusSeconds(billion)
    }
}
