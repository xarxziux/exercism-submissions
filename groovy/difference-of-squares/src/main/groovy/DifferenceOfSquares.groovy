class DifferenceOfSquares {
    private final int squareOfSumVal
    private final int sumOfSquaresVal

    DifferenceOfSquares(int n) {
        squareOfSumVal = (n**4 + 2 * n**3 + n**2) / 4
        sumOfSquaresVal = (n * (n + 1) * (2 * n + 1)) / 6
    }

    int squareOfSum() {
        squareOfSumVal
    }

    int sumOfSquares() {
        sumOfSquaresVal
    }

    int difference() {
        squareOfSumVal - sumOfSquaresVal
    }
}
