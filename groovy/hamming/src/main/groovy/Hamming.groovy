class Hamming {
    static int distance(s1, s2) {
        final int l1 = s1.length()

        if(l1 != s2.length()) {
            throw new ArithmeticException("leftStrand and rightStrand must be of equal length.")
        }

        (0..<l1).count {
            s1[it] != s2[it]
        }
    }
}
