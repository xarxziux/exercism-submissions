package Raindrops;
use strict;
use warnings;
use Exporter 'import';
our @EXPORT_OK = qw(raindrop);

# This function is a generic sound builder, facitating
# the easy creation of individual sounds without additional
# boilerplate.
sub build_sound {
    my $divisor = shift;
    my $sound = shift;

    return sub {
        my ($num) = shift;
        return ($num % $divisor == 0)
            ? $sound
            : '';
    }
}

# Now the individual sounds are a one-liner.
# Any arbitrary number can be created the same way.
*pling = build_sound(3, 'Pling');
*plang = build_sound(5, 'Plang');
*plong = build_sound(7, 'Plong');

sub raindrop {
    my ($num) = @_;
    my $sounds = pling($num) . plang($num) . plong($num);

    return $sounds || $num;
};

1;
