package Prime;
use strict;
use warnings;

sub new {
    my $class = shift;
    my $self = {
        _val => shift,
    };

    bless $self, $class;
    return $self;
};

sub factors {
    my ($self) = @_;
    $self->{_answer} = divide_by $self->{_val};
    return $self->{_answer};
};

sub increment_divisor {
    my ($divisor) = @_;
    return 3 if ($divisor == 2);
    return $divisor + 2;
}

sub divide_by {
    my $divisor = 2;
    my $val = shift;
    my @factors = @_;

    while ($val > 1) {
        if ($val < $divisor ** 2) {
            push @factors, $val;
            $val = 1;
        } elsif ($val % $divisor == 0) {
            push @factors, $divisor;
            $val /= $divisor;
        } else {
            $divisor = increment_divisor($divisor);
        }
    }

    return @factors;
}

sub test {
    my ($base) = @_;
    my @results = divide_by($base);
    print "@results\n";
};

sub test_all {
    test(1);
    test(2);
    test(3);
    test(4);
    test(6);
    test(8);
    test(9);
    test(27);
    test(625);
    test(901255);
    test(93819012551);
}

test_all();

1;
