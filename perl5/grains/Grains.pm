package Grains;
use strict;
use warnings;
use Exporter 'import';
use bigint;
our @EXPORT_OK = qw(grains_on_square total_grains);

sub grains_on_square {
    my ($square) = @_;
    if ($square < 1 || $square > 64) {
        die "square must be between 1 and 64";
    } else {
        return 1 << $square - 1;
    }
}

sub total_grains {
  return 1 << 64
}

1;
