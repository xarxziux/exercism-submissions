#!/usr/bin/env perl

package Squares;
use strict;
use warnings;

sub new {
    my $class = shift;
    my $x = shift;

    my $self = {
        _val => $x,
    };

    $self->{_sum} = ($x * ($x + 1) * (2 * $x + 1)) / 6;
    $self->{_square} = (($x**4) + 2 * ($x**3) + ($x**2)) / 4;
    $self->{_diff} = $self->{_square} - $self->{_sum};

    bless $self, $class;
    return $self;
}

sub sum_of_squares {
    my ($self) = @_;
    return $self->{_sum};
}

sub square_of_sum {
    my ($self) = @_;
    return $self->{_square};
}

sub difference {
    my ($self) = @_;
    return $self->{_diff};
}

1;
