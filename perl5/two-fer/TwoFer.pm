package TwoFer;
use strict;
use warnings;
use Exporter 'import';
use experimental qw(signatures);
our @EXPORT_OK = qw(two_fer);

sub two_fer ($name = "you") {
    "One for $name, one for me."
}

1;
