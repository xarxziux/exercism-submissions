package Hamming;
use strict;
use warnings;
use Exporter 'import';
our @EXPORT_OK = qw(hamming_distance);

sub hamming_distance {
    my ($strand1, $strand2) = @_;
    my $len = length($strand1);
    my $count = 0;

    die "left and right strands must be of equal length"
        if (length($strand2) != $len);

    for my $i (0 .. $len) {
        my $c1 = substr $strand1, $i, 1;
        my $c2 = substr $strand2, $i, 1;

        $count++ if $c1 ne $c2;
    }

    return $count;
}

1;
