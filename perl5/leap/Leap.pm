# Declare package 'Leap'
package Leap;
use strict;
use warnings;
use Exporter 'import';
use constant {
    TRUE => 1,
    FALSE => 0
};

our @EXPORT_OK = qw(is_leap_year);

sub is_leap_year_1 {
    my ($year) = @_;

    return FALSE if $year % 4 != 0;
    return TRUE if $year % 400 == 0;
    return FALSE if $year % 100 == 0;
    return TRUE;
};

sub is_leap_year_2 {
    my ($year) = @_;

    return $year % 4 == 0;# or (
        #$year % 4 == 0);# and
        #$year % 100 != 0);
};

sub is_leap_year {
    my ($year) = @_;
    return is_leap_year_2 $year;
};

if (is_leap_year(1996)) {
    print "True";
} else {
    print "False";
};

1;
