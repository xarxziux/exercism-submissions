package Bob;
use strict;
use warnings;
use Exporter 'import';
use experimental qw(signatures);
use constant {
    CALM => "Calm down, I know what I'm doing!",
    SURE => "Sure.",
    WHAT => "Whatever.",
    WHOA => "Whoa, chill out!",
    FINE => "Fine. Be that way!",
};

our @EXPORT_OK = qw(hey);

sub is_question ($str = "") {
    substr ($str, -1) eq "?"
}

sub is_shout ($str = "") {
    $str =~ /[A-Z]/ and not ($str =~ /[a-z]/)
}

sub hey ($msg = "") {
    $msg =~ s/^\s+|\s+$//g;
    my $q = is_question $msg;
    my $s = is_shout $msg;

    if ($q and $s) {
        return CALM;
    }

    if ($q) {
        return SURE;
    }

    if ($s) {
        return WHOA;
    }

    if ($msg eq "") {
        return FINE;
    }

    return WHAT;
}

1;
