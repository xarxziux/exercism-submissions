#ifndef ARMSTRONG_NUMBERS_H
#define ARMSTRONG_NUMBERS_H

#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <stddef.h>

bool is_armstrong_number(int candidate);
int get_armstrong_value(int val, int power, int accum);

#endif
