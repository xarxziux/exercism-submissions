#include "armstrong_numbers.h"

bool is_armstrong_number(int candidate)
{
  int length = snprintf(NULL, 0,"%d", candidate);
  int armstrong_value = get_armstrong_value(candidate, length, 0);
  return armstrong_value == candidate;
}

int get_armstrong_value(int val, int power, int accum)
{
  if (val < 10)
    {
      return accum+pow(val, power);
    }

  return get_armstrong_value(val/10, power, accum+pow(val % 10, power));
}
