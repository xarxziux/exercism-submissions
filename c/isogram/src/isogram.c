#include <stdbool.h>
#include <ctype.h>

bool is_isogram(const char phrase[]) {
  if (!phrase) {
    return false;
  }

  bool charArr[26] = {false};

  for(unsigned int i = 0; phrase[i] != 0; i++) {
    const char j = phrase[i];

    if (isalpha(j)) {
      const int k = tolower(j) - 'a';

      if (charArr[k]) {
        return false;
      }

      charArr[k] = true;
    }
  }

  return true;
}
