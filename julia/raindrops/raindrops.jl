#=
  This submission is intended to be generic.  Rather than hard-code
  the required raindrop definitions in, they're stored inside an array
  which is then mapped over.  In theory, you could easily add other
  definitions to this by adding it to the raindops array, with no
  other modification to the code
=#

"Data structure for rain sound definitions"
Sound = Tuple{Int, AbstractString}

"The main raindrop array"
sounds = Sound[
  (3, "Pling"),
  (5, "Plang"),
  (7, "Plong")
]

"Gets the sound of a single raindrop"
getsound(x::Int) = (t::Sound) -> (x % t[1] == 0) ? t[2] : ""

"Maps over the raindrops array to get all sounds"
getallsounds(x::Int, a::Vector{Sound}) = reduce(*, (map(getsound(x), a)))

"Returns the second of two arguments if the first is an empty string"
pickstring(s1::AbstractString, s2::AbstractString) = s1 == "" ? s2 : s1

"Convert a number to it's raindrop sounds"
raindrops(x::Int) = pickstring(getallsounds(x, sounds), string(x))
