# Convert char to int (0 to 25) regardless of case
chartoint(c::Char) = Int(c - 65) & 31

# Get the offset of the shift mod 26
offset(n::Int, c::Char) = mod(chartoint(c) + n, 26)

# Curried function for shifting a character
rotateone(n::Int) = (c::Char) -> isletter(c) ? c - chartoint(c) + offset(n, c) : c

# Translate a char or string by a given offset
rotate(n::Int, c::Char) = rotateone(n)(c)
rotate(n::Int, s::AbstractString) = map(rotateone(n), s)
