function count_nucleotides(strand::AbstractString)
    base_dict = Dict{Char,Int}('A' => 0, 'C' => 0, 'G' => 0, 'T' => 0)

    for char in strand
        if haskey(base_dict, char)
            base_dict[char] += 1
        else
            throw(DomainError(char, "invalid character"))
        end
    end
    base_dict
end
