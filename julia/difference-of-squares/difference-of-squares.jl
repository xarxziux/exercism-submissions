"Sum the squares of the numbers up to the given number"
square_of_sum(n::Int) = ((n ^ 4) + 2 * (n ^ 3) + (n ^2)) / 4

"Square the sum of the numbers up to the given number"
sum_of_squares(n::Int) = (n * (n + 1) * (2 * n + 1)) / 6

"Subtract sum of squares from square of sums"
difference(n) = square_of_sum(n) - sum_of_squares(n)
