ispangram(str) = length(setdiff('a':'z', lowercase(str))) == 0
