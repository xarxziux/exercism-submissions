"Base handshake values"
operations = Vector{AbstractString}([
    "wink", "double blink", "close your eyes", "jump"])

"Predicate indicating if a given bit is set within a number"
isbitset(n::Int) = (b::Int) -> n & (2 ^ b) == (2 ^ b)

"Filter the operations vector by a Boolean vector based on isbitset()"
handshake(n::Int) = Vector{String}(operations[map(isbitset(n), 0:3)])

"Does the answer need to be reversed?"
reversecodes(n::Int, v::Vector{String}) = isbitset(n)(4) ? reverse(v) : v

"Convert the input number into a vector of operations"
secret_handshake(code::Integer) = reversecodes(code, handshake(code))
