randletter() = rand('A':'Z')
randnumber() = rand(0:9)
randname() = string(
    randletter(), randletter(), randnumber(), randnumber(), randnumber())

mutable struct Robot
    name::AbstractString
end

Robot() = Robot(randname())

reset!(instance::Robot) = instance.name = randname
