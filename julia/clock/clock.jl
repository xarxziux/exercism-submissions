using Date

struct Clock
  hours::Int
  mins::Int
  Clock(hours, mins) = (mins < 0
    ? Clock(hours, mod(mins, 24*60))
    : new(
      mod(hours + div(mins, 60), 24),
      mod(mins, 60)))
end

+(c::Clock, m::Minute) = Clock(c.hours, c.mins + m)
-(c::Clock, m::Minute) = Clock(c.hours, c.mins - m)
