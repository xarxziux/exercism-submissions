/** @format */

const startsWithVowel = str => /^[aeiou]/i.test(str);
const startsWithQU = str => /^qu/i.test(str);

const pigLatinRecur = (head, tail = '') =>
    startsWithVowel(head)
        ? head + tail + 'ay'
        : startsWithQU(head)
            ? head.slice(2) + tail + 'quay'
            : pigLatinRecur(head.slice(1), tail + head[0]);

const pigLatin = str => pigLatinRecur(str, '');

const translate = str =>
    str
        .split(' ')
        .map(pigLatin)
        .join(' ');

module.exports = {
    translate
};
