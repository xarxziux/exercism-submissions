/** @format */

const validate = x =>
    x ===
    ('' + x)
        .split('')
        .map((e, _, {length}) => Math.pow(e, length))
        .reduce((a, b) => a + b);

module.exports = {validate};
