/** @format */

const bob = function () {};

bob.prototype.hey = _message => {

    const message = _message.trim();
    const isUpperCase =
        message === message.replace(/[a-z]/g, '') &&
        message !== message.replace(/[A-Z]/g, '');
    const isQuestion = message.slice(-1) === '?';
    const isEmpty = message === '';

    // prettier-ignore
    return isEmpty
        ? 'Fine. Be that way!'
        : isUpperCase && isQuestion
        ? 'Calm down, I know what I\'m doing!'
        : isUpperCase
        ? 'Whoa, chill out!'
        : isQuestion
        ? 'Sure.'
        : 'Whatever.';

};

module.exports = bob;
