/** @format */

const say = require('./say.js');

const show = x => {

    console.log(x + ' - ' + say(x));

};

show(1);
show(17);
show(55);
show(80);
show(100);
show(108);
show(150);
show(629);
show(700);
show(832);
show(999);
