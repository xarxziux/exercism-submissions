/** @format */

const singles = [
    '',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen'
];

const tens = [
    '',
    '',
    'twenty',
    'thirty',
    'fourty',
    'fifty',
    'sixty',
    'seventy',
    'eighty',
    'ninety'
];

const thousands = [
    'thousand',
    'million',
    'billion',
    'trillion',
    'quadrillion'
];

const splitThousands = (str, accum = []) =>
    str === ''
        ? accum
        : str.length < 3
            ? accum.concat(str - 0)
            : splitThousands(
                str.slice(0, -3),
                accum.concat(str.slice(-3) - 0)
            );

const sayHundreds = x => {

    if (x < 20) return singles[x];

    if (x < 100) {

        if (x % 10 === 0) return tens[Math.floor(x / 10)];

        return tens[Math.floor(x / 10)] + '-' + sayHundreds(x % 10);
    
    }

    const sub = sayHundreds(x % 100);

    return (
        singles[Math.floor(x / 100)] +
        ' hundred' +
        (sub === '' ? '' : ' and ' + sub)
    );

};

const say = x =>
    splitThousands('' + x)
        .map(sayHundreds)
        .foldRight((a, b) => a + b);

module.exports = say;
