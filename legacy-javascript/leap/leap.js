/** @format */

//
// This is only a SKELETON file for the "Leap" exercise. It's been
// provided as a convenience to get you started writing code faster.
//

const isDivisibleBy = mod => div => div % mod === 0;
const isDivisibleBy4 = isDivisibleBy(4);
const isDivisibleBy100 = isDivisibleBy(100);
const isDivisibleBy400 = isDivisibleBy(400);

const isLeapYear = year =>
    isDivisibleBy400(year) ||
    (isDivisibleBy4(year) && !isDivisibleBy100(year));

function Year (year) {

    this.isLeapInternal = isLeapYear(year);
    this.isLeap = function () {

        return this.isLeapInternal;
    
    };

}

// Year.prototype.isLeap = year;

module.exports = Year;
