/** @format */

let Minesweeper = require('./minesweeper');

describe('Minesweeper()', function () {

    it('handles no rows', function () {

        let minesweeper = new Minesweeper();
        expect(minesweeper.annotate([])).toEqual([]);
    
    });

    xit('handles no columns', function () {

        let minesweeper = new Minesweeper();
        expect(minesweeper.annotate([''])).toEqual(['']);
    
    });

    xit('handles no mines', function () {

        let minesweeper = new Minesweeper();
        let input = ['   ', '   ', '   '];
        let expected = ['   ', '   ', '   '];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles board with only mines', function () {

        let minesweeper = new Minesweeper();
        let input = ['***', '***', '***'];
        let expected = ['***', '***', '***'];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles mine surrounded by spaces', function () {

        let minesweeper = new Minesweeper();
        let input = ['   ', ' * ', '   '];
        let expected = ['111', '1*1', '111'];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles space surrounded by mines', function () {

        let minesweeper = new Minesweeper();
        let input = ['***', '* *', '***'];
        let expected = ['***', '*8*', '***'];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles space surrounded by mines', function () {

        let minesweeper = new Minesweeper();
        let input = ['***', '* *', '***'];
        let expected = ['***', '*8*', '***'];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles horizontal line', function () {

        let minesweeper = new Minesweeper();
        let input = [' * * '];
        let expected = ['1*2*1'];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles horizontal line, mines at edges', function () {

        let minesweeper = new Minesweeper();
        let input = ['*   *'];
        let expected = ['*1 1*'];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles vertical line', function () {

        let minesweeper = new Minesweeper();
        let input = [' ', '*', ' ', '*', ' '];
        let expected = ['1', '*', '2', '*', '1'];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles vertical line, mines at edges', function () {

        let minesweeper = new Minesweeper();
        let input = ['*', ' ', ' ', ' ', '*'];
        let expected = ['*', '1', ' ', '1', '*'];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles cross', function () {

        let minesweeper = new Minesweeper();
        let input = ['  *  ', '  *  ', '*****', '  *  ', '  *  '];
        let expected = [' 2*2 ', '25*52', '*****', '25*52', ' 2*2 '];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

    xit('handles large board', function () {

        let minesweeper = new Minesweeper();
        let input = [
            ' *  * ',
            '  *   ',
            '    * ',
            '   * *',
            ' *  * ',
            '      '
        ];
        let expected = [
            '1*22*1',
            '12*322',
            ' 123*2',
            '112*4*',
            '1*22*2',
            '111111'
        ];
        expect(minesweeper.annotate(input)).toEqual(expected);
    
    });

});
