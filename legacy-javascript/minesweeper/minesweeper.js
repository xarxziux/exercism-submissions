/** @format */

const arrToGrid = arr => ({
    data: arr.join('').split(''),
    width: arr[0].length
});

const gridToArr = ({data, width}, accum = []) =>
    data.length <= width
        ? accum.concat(data.join(''))
        : gridToArr(
            {
                data: data.slice(width),
                width
            },
            accum.concat(data.slice(0, width).join(''))
        );

const isMine = x => x === '*';

const countNearbyMines = ({data, width}) => index =>
    0 +
    isMine([index - width - 1]) +
    isMine(data[index - width]) +
    isMine(data[index - width + 1]) +
    isMine(data[index - 1]) +
    isMine(data[index + 1]) +
    isMine(data[index + width - 1]) +
    isMine(data[index + width]) +
    isMine(data[index + width + 1]);

const annotateEach = grid => (_, index) => {

    const countInternal = countNearbyMines(grid);

    if (isMine(grid.data[index])) return '*';

    const mineCount = countInternal(index);

    return mineCount === 0 ? ' ' : mineCount;

};

const annotateAll = grid => grid.data.map(annotateEach(grid));

const annotate = str => {

    return gridToArr(annotateAll(arrToGrid(str)));

};
