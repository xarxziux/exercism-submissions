/** @format */

const dropHyphens = str => str.replace(/-/g, '');
const isValidISBNShape = str => /^\d-?\d{3}-?\d{5}-?[\d|X]$/.test(str);
const strToArr = str => str.split('');

const foldArr = (accum, current, index) =>
    current === 'X'
        ? accum + 10 * (10 - index)
        : accum + current * (10 - index);

const isValidISBN = str =>
    isValidISBNShape(str) &&
    strToArr(dropHyphens(str)).reduce(foldArr, 0) % 11 === 0;

module.exports = function ISBN (isbnStr) {

    const isValid = isValidISBN(isbnStr);
    this.isValid = function () {

        return isValid;
    
    };

};
