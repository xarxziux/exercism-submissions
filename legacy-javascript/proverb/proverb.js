/** @format */

let pullQualifier = arr => {

    let lastElem = arr[arr.length - 1];

    return typeof lastElem === 'string'
        ? {arr, qualifier: ''}
        : {arr: arr.slice(0, -1), qualifier: lastElem.qualifier + ' '};

};

let buildEnding = (item, qualifier) =>
    `And all for the want of a ${qualifier}${item}.`;

let buildLine = (str1, str2) =>
    `For want of a ${str1} the ${str2} was lost.\n`;

let buildLines = (arr, accum = '') =>
    arr.length < 2
        ? accum
        : buildLines(arr.slice(1), accum + buildLine(arr[0], arr[1]));

let buildProverb = ({arr, qualifier}) =>
    buildLines(arr) + buildEnding(arr[0], qualifier);

module.exports = function Proverb (...initArr) {

    let proverb = buildProverb(pullQualifier(initArr));

    this.toString = function () {

        return proverb;
    
    };

};
