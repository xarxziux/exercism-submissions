/** @format */

const isValidKey = key => /^[a-z]+$/.test(key);
const getRandInt = max => () => Math.floor(Math.random() * max);

const mod = limit => x => (x % limit + limit) % limit;
const mod26 = mod(26);
const charToNum = x => x.charCodeAt() - 97;
const numToChar = n => String.fromCharCode(n + 97);
const strToArr = str => str.split('').map(charToNum);
const arrToStr = arr => arr.map(numToChar).join('');

const getRandomKey = () =>
    arrToStr(
        Array(100 + getRandInt(100)())
            .fill(null)
            .map(getRandInt(26))
    );

const xcodeElem = mult => keyArr => (elem, index) =>
    mod26(elem + keyArr[index % keyArr.length] * mult);

const encodeElem = xcodeElem(1);
const decodeElem = xcodeElem(-1);

function Cipher(key = getRandomKey()) {
    if (!isValidKey(key)) throw new Error('Bad key');

    this.key = key;
}

Cipher.prototype.encode = function(str) {
    return arrToStr(strToArr(str).map(encodeElem(strToArr(this.key))));
};

Cipher.prototype.decode = function(str) {
    return arrToStr(strToArr(str).map(decodeElem(strToArr(this.key))));
};

module.exports = Cipher;
