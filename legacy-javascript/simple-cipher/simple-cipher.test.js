/** @format */

const Cipher = require('./simple-cipher');

// const c1 = new Cipher();
// const c2 = new Cipher();
const ca = new Cipher('a');
const caaa = new Cipher('aaaaaaaaaa');
const cabc = new Cipher('abc');

/* eslint-disable no-console */
// console.log(c1.key);
// console.log(c2.key);

// console.log(ca.key);
console.log(ca.encode('abcdefghi'));
// console.log(caaa.key);
console.log(caaa.encode('abcdefghi'));
console.log(cabc.encode('iiiiiiiiiiiiiii'));
console.log(cabc.decode('ijkijkijkijkijkijki'));

// const cerr = new Cipher('H');
