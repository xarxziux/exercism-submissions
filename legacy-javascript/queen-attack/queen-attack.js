/** @format */

const onSameSquare = (white, black) =>
    white[0] === black[0] && white[1] === black[1];
const onSameRank = (white, black) => white[0] === black[0];
const onSameFile = (white, black) => white[1] === black[1];
const onSameDiag = (white, black) =>
    Math.abs(black[0] - white[0]) === Math.abs(black[1] - white[1]);
const canAttack = (white, black) =>
    onSameRank(white, black) ||
    onSameFile(white, black) ||
    onSameDiag(white, black);

const setGrid = (white, black) => {

    const grid = Array(64).fill('_ ');
    grid[white[0] * 8 + white[1]] = 'W ';
    grid[black[0] * 8 + black[1]] = 'B ';
    return grid.join('');

};

const splitGrid = gridStr =>
    gridStr.slice(0, 15) +
    '\n' +
    gridStr.slice(16, 31) +
    '\n' +
    gridStr.slice(32, 47) +
    '\n' +
    gridStr.slice(48, 63) +
    '\n' +
    gridStr.slice(64, 79) +
    '\n' +
    gridStr.slice(80, 95) +
    '\n' +
    gridStr.slice(96, 111) +
    '\n' +
    gridStr.slice(112, 127) +
    '\n';

function Queens ({white, black} = {white: [0, 3], black: [7, 3]}) {

    this.white = white || [0, 3];
    this.black = black || [7, 3];

    if (onSameSquare(this.white, this.black))
        throw new Error('Queens cannot share the same space');

}

Queens.prototype.canAttack = function () {

    return canAttack(this.white, this.black);

};

Queens.prototype.toString = function () {

    return splitGrid(setGrid(this.white, this.black));

};

module.exports = Queens;
