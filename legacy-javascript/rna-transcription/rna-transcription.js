/** @format */

const throwError = msg => {

    throw new Error(msg);

};

const translate = {
    G: 'C',
    C: 'G',
    T: 'A',
    A: 'U'
};

const toRna = (dna, rna = '') =>
    dna === ''
        ? rna
        : toRna(
            dna.slice(1),
            rna + (translate[dna[0]] || throwError('Invalid input'))
        );

function DnaTranscriber () {}

DnaTranscriber.prototype.toRna = function (dnaStr) {

    return toRna(dnaStr, '');

};

module.exports = DnaTranscriber;
