/** @format */

const arrToObj = score => (accum, next) =>
    Object.assign({}, accum, {[next.toLowerCase()]: score - 0});

const propToObj = (score, charList) => charList.reduce(arrToObj(score), {});

const allPropsToObj = scoreObj => {

    let accum = {};

    for (const nextScore in scoreObj)
        if (scoreObj.hasOwnProperty(nextScore))
            Object.assign(accum, propToObj(nextScore, scoreObj[nextScore]));

    return accum;

};

function ETL () {}

ETL.prototype.transform = function (scoreObj) {

    return allPropsToObj(scoreObj);

};

module.exports = ETL;
