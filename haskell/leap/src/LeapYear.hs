module LeapYear (isLeapYear) where

isDivisibleBy :: Integer -> Integer -> Bool
isDivisibleBy div num = mod num div == 0

isLeapYear :: Integer -> Bool
isLeapYear year
  | isDivisibleBy 400 year = True
  | isDivisibleBy 100 year = False
  | otherwise = isDivisibleBy 4 year
