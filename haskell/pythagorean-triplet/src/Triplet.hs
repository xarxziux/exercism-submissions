module Triplet (isPythagorean, mkTriplet, pythagoreanTriplets) where

tupleMax :: (Int, Int, Int) -> Int
tupleMax (x, y, z) = max ([x, y, z])

isPythagorean :: (Int, Int, Int) -> Bool
isPythagorean (x, y, z) =
    (x ^ 2 + y ^ 2 == z ^ 2) ||
    (x ^ 2 + z ^ 2 == y ^ 2) ||
    (y ^ 2 + z ^ 2 == x ^ 2)

mkTriplet :: Int -> Int -> Int -> (Int, Int, Int)
mkTriplet a b c = (a, b, c)

pythagoreanTriplets :: Int -> Int -> [(Int, Int, Int)]
pythagoreanTriplets a b = [(a, b, a ^ 2 + b ^ 2)]
