module Acronym (abbreviate) where

import Data.Char (toUpper, isUpper, isLower, isAlpha)

isHead :: (Char, Char) -> Bool
isHead (x, y) = (isLower x && isUpper y) || (elem x " -" && isAlpha y)

abbreviate :: String -> String
abbreviate x = [toUpper b | (a, b) <- zip (' ':x) x, isHead (a, b)]
