module Allergies (Allergen(..), allergies, isAllergicTo) where
import Data.Bits

data Allergen = Eggs
              | Peanuts
              | Shellfish
              | Strawberries
              | Tomatoes
              | Chocolate
              | Pollen
              | Cats
              deriving (Eq)

--allergy_list :: [Allergen]
--allergy_list = [Eggs, Peanuts, Shellfish, Strawberries,
--  Tomatoes, Chocolate, Pollen, Cats]

allergies :: Int -> [Allergen]
allergies score = error "You need to implement this function."

isAllergicTo :: Allergen -> Int -> Bool
isAllergicTo allergen score = case allergen of
  Eggs -> (.&.) score 1 == 1
  Peanuts -> (.&.) score 2 == 2
  Shellfish -> (.&.) score 4 == 4
  Strawberries -> (.&.) score 8 == 8
  Tomatoes -> (.&.) score 16 == 16
  Chocolate -> (.&.) score 32 == 32
  Pollen -> (.&.) score 64 == 64
  Cats -> (.&.) score 128 == 128
