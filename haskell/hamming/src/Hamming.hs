module Hamming (distance) where

sameChar :: Char -> Char -> Int
sameChar x y
    | x == y = 0
    | otherwise = 1

distance :: String -> String -> Maybe Int
distance x y
    | length x /= length y = Nothing
    | otherwise = Just . sum $ zipWith sameChar x y
