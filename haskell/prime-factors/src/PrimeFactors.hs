module PrimeFactors (primeFactors) where

flattenOdd :: Integer -> Integer -> [Integer] -> [Integer]
flattenOdd 1 _ a = a
flattenOdd c d a
  | d * 2 > c    = a ++ [c]
  | rem c d == 0 = flattenOdd (quot c d) d (a ++ [d])
  | otherwise    = flattenOdd c (d + 2) a

flattenEven :: Integer -> [Integer] -> [Integer]
flattenEven 1 a = a
flattenEven c a
  | rem c 2 == 0 = flattenEven (quot c 2) (2:a)
  | otherwise    = flattenOdd c 3 a

primeFactors :: Integer -> [Integer]
primeFactors n = flattenEven n []
