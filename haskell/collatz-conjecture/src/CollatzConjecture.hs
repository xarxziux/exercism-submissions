module CollatzConjecture (collatz) where

collatzCount :: Integer -> Integer -> Integer
collatzCount x y
    | x == 1        = y
    | mod x 2 == 0  = collatzCount (div x 2) (y + 1)
    | otherwise     = collatzCount (x * 3 + 1) (y + 1)

collatz :: Integer -> Maybe Integer
collatz x
    | x <= 0 = Nothing
    | otherwise = Just $ collatzCount x 0
