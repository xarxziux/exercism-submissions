module SumOfMultiples (sumOfMultiples) where

isDivisibleBy :: Integer -> Integer -> Bool
isDivisibleBy n d = n `rem` d == 0

isDivList :: [Integer] -> Integer -> Bool
isDivList xs num = any (isDivisibleBy num) xs

sumOfMultiples :: [Integer] -> Integer -> Integer
sumOfMultiples factors limit =
  sum $ filter (isDivList factors) [1..(limit-1)]
