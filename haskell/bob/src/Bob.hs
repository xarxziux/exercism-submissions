module Bob (responseFor) where
import Data.Char (isLower, isUpper)
import qualified Data.Text as T

isEmpty :: String -> Bool
isEmpty "" = True
isEmpty str = False

isQuestion :: String -> Bool
isQuestion "" = False
isQuestion str = last str == '?'

isShout :: String -> Bool
isShout str = any isUpper str && (not $ any isLower str)

responseFor :: String -> String
responseFor xs
    | isEmpty xs_trim = "Fine. Be that way!"
    | isQuestion xs_trim && isShout xs_trim = "Calm down, I know what I\'m doing!"
    | isQuestion xs_trim = "Sure."
    | isShout xs_trim = "Whoa, chill out!"
    | otherwise = "Whatever."
    where
        xs_trim = T.unpack . T.strip $ T.pack xs
