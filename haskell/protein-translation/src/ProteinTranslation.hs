module ProteinTranslation(proteins) where

getProtein :: String -> String
getProtein "AUG" = "Methionine"
getProtein "UUU" = "Phenylalanine"
getProtein "UUC" = "Phenylalanine"
getProtein "UUA" = "Leucine"
getProtein "UUG" = "Leucine"
getProtein "UCU" = "Serine"
getProtein "UCC" = "Serine"
getProtein "UCA" = "Serine"
getProtein "UCG" = "Serine"
getProtein "UAU" = "Tyrosine"
getProtein "UAC" = "Tyrosine"
getProtein "UGU" = "Cysteine"
getProtein "UGC" = "Cysteine"
getProtein "UGG" = "Tryptophan"
getProtein "UAA" = "STOP"
getProtein "UAG" = "STOP"
getProtein "UGA" = "STOP"
getProtein _ = ""

translate :: String -> [Maybe String]
translate [] = []
translate (c1:c2:c3:x)
  | protein ==  "STOP" = []
  | protein == "" = [Nothing]
  | otherwise = Just protein : translate x
    where protein = getProtein [c1, c2, c3]
translate _ = [Nothing]

proteins :: String -> Maybe [String]
proteins s = sequence $ translate s
