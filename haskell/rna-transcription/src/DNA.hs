module DNA (toRNA) where

toRNAElem :: Char -> Maybe Char
toRNAElem x = case x of
    'G' -> Just 'C'
    'C' -> Just 'G'
    'T' -> Just 'A'
    'A' -> Just 'U'
    _   -> Nothing

toRNA :: String -> Maybe String
toRNA str = sequence $ map toRNAElem str
