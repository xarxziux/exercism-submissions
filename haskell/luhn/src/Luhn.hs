module Luhn (isValid) where

doubleDigit :: Char -> Int
doubleDigit c = case c of
  '0' -> '0'
  '1' -> '2'
  '2' -> '4'
  '3' -> '6'
  '4' -> '8'
  '5' -> '1'
  '6' -> '3'
  '7' -> '5'
  '8' -> '7'
  '9' -> '9'



isValid :: String -> Bool
isValid n = error "You need to implement this function."
