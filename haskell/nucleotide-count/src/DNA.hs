module DNA (nucleotideCounts, Nucleotide(..)) where

import Data.Map (Map, fromList, adjust)
import Data.Either (Either (Left, Right))
import Control.Monad (foldM)

data Nucleotide = A | C | G | T
    deriving (Show, Read, Ord, Eq)

nucString :: String
nucString = "ACGT"

countChar :: Map Nucleotide Int -> Char -> Either String (Map Nucleotide Int)
countChar m n
    | n `elem` nucString = Right $ adjust (+ 1) (read [n] :: Nucleotide) m
    | otherwise = Left "Invalid character"

baseMap :: Map Nucleotide Int
baseMap = fromList [(A, 0), (C, 0), (G, 0), (T, 0)]

nucleotideCounts :: String -> Either String (Map Nucleotide Int)
nucleotideCounts = foldM countChar baseMap
