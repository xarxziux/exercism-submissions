#!/bin/bash

param="$1"
p_length=${#param}

for (( c = 0; c < p_length; c++ ))
do
    (( next = ${param:c:1}**p_length ))
    (( count += next ))
done

if [[ ${count} -eq ${param} ]]
then
    echo "true"
else
    echo "false"
fi

exit 0
