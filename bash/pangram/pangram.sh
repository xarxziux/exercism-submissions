#!/usr/bin/env bash

if [[ $# -lt 1 ]]
then
    echo "Usage: pangram.sh <string>"
    exit 1
fi

i_lower=$(echo $1 | tr A-Z a-z)
remaining=$(echo abcdefghijklmnopqrstuvwxyz | tr -d "${i_lower}")

if [[ ${#remaining} == 0 ]]
then
  echo "true"
else
  echo "false"
fi

exit 0
