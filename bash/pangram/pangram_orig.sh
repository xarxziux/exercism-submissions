#!/usr/bin/env bash

if [[ $# -lt 1 ]]
then
    echo "Usage: pangram.sh <string>"
    exit 1
fi

str=${1,,}
accum=""

for (( i=0; i < ${#str}; i++ ))
do
  next=${str:i:1}
  if [[ ${next} == [a-z] ]]
  then
    if [[ ${accum} != *${next}* ]]
    then
      accum=${accum}${next}
    fi
  fi
done

if [[ ${#accum} == 26 ]]
then
  echo "true"
else
  echo "false"
fi

exit 0
