#!/usr/bin/env bash

script_version="0.0.2"
script_name=$( basename "$0" )
code_type=0
input="$2"
usage="Usage: ${script_name} [OPTIONS] encode|decode INPUT"

# Parse the command line parameters
parse_command_line() {
    while [[ $# -gt 0 ]]
    do
        param="$1"
        case "${param}" in
            -v|--version)
                echo "${script_name}" version ${script_version}
                echo "Copyright (C) xarxziux 2018"
                exit 0
                ;;
            -h|--help)
                show_help=1
                shift
                ;;
            encode)
                code_type=1
                shift
                input="$1"
                shift
                ;;
            decode)
                code_type=2
                shift
                input="$1"
                shift
                ;;
            *)
                echo "${script_name}": unknown option "${param}"
                echo "${usage}"
                exit 0
                ;;
        esac
    done
}

# Recursively split the output string into chunks of five characters
split_string() {
    param=$1
    if [[ "${#param}" -le 5 ]]
    then
        echo "$param"
    else
        head="${param:0:5}"
        tail=$( split_string "${param:5}" )
        echo "${head} ${tail}"
    fi
}

main() {
    parse_command_line "$@"

    # Show the help screen
    if [[ ${show_help} -eq 1 ]]
    then
        echo "${usage}"
        echo "Encode or decode a string using the Atbash cipher"
        echo
        echo "Options:"
        echo "  -h, --help                    display this help and exit"
        echo "  -v, --version                 display the version number and exit"
        echo ""
        echo "encode INPUT                    encode the given INPUT string with the Atbash cipher"
        echo "decode INPUT                    decode the given INPUT string with the Atbash cipher"
        return 0
    fi

    # Verify that we have valid parameters
    if [[ ${code_type} -eq 0 ]]
    then
        echo "Missing encode|decode command line parameter"
        echo "${usage}"
        return 1
    elif [[ -z ${input} ]]
    then
        echo "Missing input string"
        echo "${usage}"
        return 1
    fi

    # Pipe and win
    output=$( \
        echo "${input}" \
            | tr --delete ' ,.' \
            | tr '[:upper:]' '[:lower:]' \
            | tr a-z zyxwvutsrqponmlkjihgfedcba)

    # The only difference between encoding and decoding is that the encoded
    # string is to be split into chunks of five
    if [[ ${code_type} -eq 2 ]]
    then
        echo "${output}"
    else
        split_string "${output}"
    fi

    return 0
}

main "$@"
exit $?
