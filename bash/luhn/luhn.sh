#!/usr/bin/env bash

to_double() {
    echo $(( ($1 - $2 - 1) % 2 ))
}

trim_double() {
    if [[ $1 -ge 10 ]]
    then
        echo $(( $1 - 9 ))
    else
        echo $1
    fi
}

input=${1//[[:space:]]}
input_len=${#input}

if [[ ${input_len} -le 1 || ! $input =~ ^[[:digit:]]+$ ]]
then
    echo "false"
    exit 0
fi

total=0

for i in $( eval echo {0..$(( input_len - 1 ))} )
do
    next=${input:i:1}
    dbl_mult=$(to_double input_len i)
    next_dbl=$(( next + (next * dbl_mult) ))
    next_trim=$(trim_double ${next_dbl})
    (( total += next_trim ))
done

(( is_luhn = total % 10 ))

if [[ ${is_luhn} -eq 0 ]]
then
    echo "true"
else
    echo "false"
fi

exit 0
