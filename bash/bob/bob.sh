#!/usr/bin/env bash

if [[ $# -lt 1 ]]
then
    echo "Fine. Be that way!"
    exit 0
fi

input=${1//[[:space:]]} 
input_len=${#input}
is_yell() {
    [[ $input =~ [A-Z] && ! $input =~ [a-z] ]]
}
is_question() {
    [[ ${input_len} -gt 0 && ${input:(-1)} == "?" ]]
}

if [[ ${input_len} -eq 0 ]]
then
  echo "Fine. Be that way!"
elif is_yell && is_question
then
  echo "Calm down, I know what I'm doing!"
elif is_yell
then
  echo "Whoa, chill out!"
elif is_question
then
  echo "Sure."
else
  echo "Whatever."
fi

exit 0
