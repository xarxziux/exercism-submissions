#!/usr/bin/env bash

if [[ $# -lt 2 ]]
then
    echo "Usage: hamming.sh <strand1> <strand2>"
    exit 1
fi

s1=$1
s2=$2
diff_count=0

if [[ ${#s1} -ne ${#s2} ]]
then
  echo "left and right strands must be of equal length"
  exit 1
fi

for (( i=0; i < ${#s1}; i++ ))
do
  if [[ ${s1:i:1} != ${s2:i:1} ]]
  then
    (( diff_count += 1))
  fi
done

echo ${diff_count}
exit 0
